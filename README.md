# AoMD-chronometer

## 必要な設定

- 環境変数: JDBC_DATABASE_URLにAoMDのゲームデータベースを指定

### Herokuの場合

- AoMD-frontと同じpostgresql、Redisサーバーをattachmentする
- 環境変数に以下を指定しないと簡単にメモリ超過する
  - `heroku config:add JAVA_OPTS="-XX:MaxRAM=500m -Xmx200m -Xss512k -XX:+UseCompressedOops -Dorg.gradle.daemon=false -Dorg.gradle.jvmargs='-XX:MaxRAM=500M -Xmx200m -Xss512k -XX:+UseCompressedOops'"`
