package net.tennana.aomd.chronometer.table;

import com.github.andrewoma.kwery.core.Session
import com.github.andrewoma.kwery.mapper.AbstractDao
import com.github.andrewoma.kwery.mapper.Table
import com.github.andrewoma.kwery.mapper.Value
import com.github.andrewoma.kwery.mapper.offsetDateTimeConverter
import net.tennana.aomd.chronometer.log.ShipActionLogRecord
import net.tennana.aomd.chronometer.log.flight.FlightBasic

object ShipActionResultTable : Table<ShipActionLogRecord, String>("ship_action_results") {
    val Id by col(ShipActionLogRecord::id, id = true)
    val ShipId by col(FlightBasic::shipId, ShipActionLogRecord::basic)
    val Turn by col(FlightBasic::turn, ShipActionLogRecord::basic)
    val Time by col(FlightBasic::time, ShipActionLogRecord::basic)
    val SupplyPower by col(FlightBasic::supplyPower, ShipActionLogRecord::basic)
    val SurplusPower by col(FlightBasic::surplusPower, ShipActionLogRecord::basic)
    val ShipName by col(FlightBasic::shipName, ShipActionLogRecord::basic)
    val SectorId by col(FlightBasic::sectorId, ShipActionLogRecord::basic)
    val x by col(FlightBasic::x, ShipActionLogRecord::basic)
    val y by col(FlightBasic::x, ShipActionLogRecord::basic)
    val StationId by col(FlightBasic::stationId, ShipActionLogRecord::basic)
    val BodyJson by col(ShipActionLogRecord::bodyString)
    val CreatedAt by col(ShipActionLogRecord::createdAt, converter = offsetDateTimeConverter)
    val UpdatedAt by col(ShipActionLogRecord::updatedAt, converter = offsetDateTimeConverter)
    override fun idColumns(id: String) = setOf(Id of id)
    override fun create(value: Value<ShipActionLogRecord>): ShipActionLogRecord {
        return ShipActionLogRecord(
            value of Id,
            FlightBasic(
                value of Turn,
                value of Time,
                value of ShipId,
                value of ShipName,
                value of SupplyPower,
                value of SurplusPower,
                value of SectorId,
                value of x,
                value of y,
                value of StationId,
            ),
            value of BodyJson,
            value of CreatedAt,
            value of UpdatedAt,
        )
    }
}

class ShipActionResultDao(session: Session) :
    AbstractDao<ShipActionLogRecord, String>(session, ShipActionResultTable, ShipActionLogRecord::id) {
    fun findByTurn(turn: Long): List<ShipActionLogRecord> {
        val name = "findByTurn"

        val sql = sql(Pair(name, turn)) {
            "select * \nfrom ${table.name}\nwhere turn = :turn order by id"
        }
        return session.select(sql, mapOf(Pair("turn", turn)), options(name), table.rowMapper(table.allColumns))
    }
}
