package net.tennana.aomd.chronometer.logic.flight

import net.tennana.aomd.chronometer.entity.Coordinate
import net.tennana.aomd.chronometer.entity.sector.map.GridMap
import net.tennana.aomd.chronometer.entity.sector.map.SearchCell
import net.tennana.aomd.chronometer.entity.sector.map.SearchMap
import java.util.ArrayList

enum class DIRECTION(val diffX: Int, val diffY: Int) {
    RIGHT(0, 1),
    LEFT(0, -1),
    TOP(1, 0),
    BOTTOM(-1, 0),
    RIGHT_TOP(1, 1),
    RIGHT_BOTTOM(-1, 1),
    LEFT_TOP(1, -1),
    LEFT_BOTTOM(-1, -1)
}

fun makeNormalRoute(map: SearchMap, from: Coordinate, to: Coordinate): ArrayList<SearchCell> {
    val start = map.getCell(from)
    start.searchPoint = 0
    val stackList = arrayListOf<SearchCell>(start)
    while (stackList.isNotEmpty()) {
        if(recursiveCost(map, stackList, to, stackList.removeAt(0))) {
            // 見つかるまで探索する
            break
        }
    }
    val goal = map.getCell(to)
    var currentCell = goal
    val route = arrayListOf<SearchCell>(goal)
    while (currentCell != start) {
        val nextMoveDiff = DIRECTION.values().minByOrNull { diff ->
            map.getCell(currentCell.coordinate.addDiff(diff)).searchPoint
        }!!
        val nextCell = map.getCell(currentCell.coordinate.addDiff(nextMoveDiff))
        route += nextCell
        currentCell = nextCell
    }
    route.reverse()
    return route
}

fun recursiveCost(map: SearchMap, stackList: ArrayList<SearchCell>, to: Coordinate, currentCell: SearchCell) : Boolean {
    DIRECTION.values().forEach {
        val nextCell = map.getCell(currentCell.coordinate.addDiff(it))
        if(nextCell.active()){
            // コスト算出済みか範囲外
           return@forEach
        }
        nextCell.searchPoint = currentCell.searchPoint + 1
        stackList += nextCell
        if(currentCell.coordinate == to){
            return@recursiveCost true
        }
    }
    return false
}
