package net.tennana.aomd.chronometer.repository

import com.github.andrewoma.kwery.core.Session
import kotlinx.serialization.Serializable
import net.tennana.aomd.chronometer.entity.Ship
import net.tennana.aomd.chronometer.entity.Universe
import net.tennana.aomd.chronometer.entity.ship.ShipComponents
import net.tennana.aomd.chronometer.entity.ship.engine.Engine
import net.tennana.aomd.chronometer.entity.ship.engine.Engines
import net.tennana.aomd.chronometer.entity.ship.generator.Generator
import net.tennana.aomd.chronometer.entity.ship.generator.Generators
import net.tennana.aomd.chronometer.table.ShipDao
import net.tennana.aomd.chronometer.table.ShipPlanDao

@Serializable
data class ShipPartsMasterRecord(
    val id: Int,
    val name: String,
    val price: Int,
    val power: Int,
    val mass: Int,
    val hp_max: Int,
    val com1: Int,
    val com2: Int,
    val com3: Int,
    private val parts_type_out: Int
) {
    val partsType: PartsType = PartsType.values().find { it.type == parts_type_out } ?: PartsType.UNKNOWN
}

enum class PartsType(val type: Int) {
    GENERATOR(2),
    ENGINE(3),
    UNKNOWN(-1)
}

object ShipRepository {
    private val shipPartsStore: CsvStore<ShipPartsMasterRecord> =
        CsvStore("master/master_ship_parts.txt", ShipPartsMasterRecord.serializer(), { it.id }, delimiter = '\t')
    private lateinit var dao: ShipDao
    private lateinit var partsDao: ShipPlanDao


    fun findAll(session: Session): List<Ship> {
        this.dao = ShipDao(session)
        this.partsDao = ShipPlanDao(session)
        val ships = this.dao.findAll()
        val parts = this.partsDao.findAll().also { list ->
            list.map {
                it.master = shipPartsStore.get(it.partId.toInt())
            }
        }.groupBy { it.shipId };
        ships.forEach {
            val generatorList = ArrayList<Generator>()
            val engineList = ArrayList<Engine>()
            parts.get(it.id)?.forEach {
                val master = it.master!!
                if (master.partsType == PartsType.ENGINE) {
                    engineList += Engine(master.com1, master.power * -1)
                }
                if (master.partsType == PartsType.GENERATOR) {
                    generatorList += Generator(master.power);
                }
            };
            val generators = Generators(generators = generatorList.toMutableList())
            val engines = Engines(engineList = engineList.toMutableList())
            it.components = ShipComponents(generators, engines)
        }
        return ships
    }

    fun commit(universe: Universe) {
        universe.ships.forEach { this.dao.unsafeUpdate(it) }
    }
}