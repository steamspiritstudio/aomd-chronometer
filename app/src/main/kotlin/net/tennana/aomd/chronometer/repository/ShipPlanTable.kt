package net.tennana.aomd.chronometer.table;

import com.github.andrewoma.kwery.core.Session
import com.github.andrewoma.kwery.mapper.AbstractDao
import com.github.andrewoma.kwery.mapper.Table
import com.github.andrewoma.kwery.mapper.Value
import com.github.andrewoma.kwery.mapper.VersionedWithTimestamp
import net.tennana.aomd.chronometer.repository.ShipPartsMasterRecord

data class ShipPlan(
    var id: Long,
    var partId: Long,
    var shipId: Long,
    var priority: Long,
    var hp: Long,
    var hpMax: Long,
    var tags: String,
    var createdAt: java.time.LocalDateTime,
    var updatedAt: java.time.LocalDateTime,
    var master: ShipPartsMasterRecord? = null
) {}

object ShipPlanTable : Table<ShipPlan, Long>("ship_plans"), VersionedWithTimestamp {
    val Id by col(ShipPlan::id, id = true)
    val PartId by col(ShipPlan::partId)
    val ShipId by col(ShipPlan::shipId)
    val Priority by col(ShipPlan::priority, default = 0) // 画面から指定できないため固定
    val Hp by col(ShipPlan::hp)
    val HpMax by col(ShipPlan::hpMax)
    val Tags by col(ShipPlan::tags)
    val CreatedAt by col(ShipPlan::createdAt)
    val UpdatedAt by col(ShipPlan::updatedAt, version = true)
    override fun idColumns(id: Long) = setOf(Id of id)
    override fun create(value: Value<ShipPlan>): ShipPlan {
        return ShipPlan(
            value of Id,
            value of PartId,
            value of ShipId,
            0,
            value of Hp,
            value of HpMax,
            value of Tags,
            value of CreatedAt,
            value of UpdatedAt,
        )
    }
}

class ShipPlanDao(session: Session) : AbstractDao<ShipPlan, Long>(session, ShipPlanTable, ShipPlan::id)
