package net.tennana.aomd.chronometer.table;

import com.github.andrewoma.kwery.core.Session
import com.github.andrewoma.kwery.mapper.AbstractDao
import com.github.andrewoma.kwery.mapper.Table
import com.github.andrewoma.kwery.mapper.Value
import com.github.andrewoma.kwery.mapper.VersionedWithTimestamp

data class SectorOfSpace(
    var id: Long,
    var name: String,
) {}

object SectorOfSpaceTable : Table<SectorOfSpace, Long>("sector_of_spaces") {
    val Id by col(SectorOfSpace::id, id = true)
    val Name by col(SectorOfSpace::name)
    override fun idColumns(id: Long) = setOf(Id of id)
    override fun create(value: Value<SectorOfSpace>): SectorOfSpace {
        return SectorOfSpace(
            value of Id,
            value of Name,
        )
    }
}

class SectorOfSpaceDao(session: Session) :
    AbstractDao<SectorOfSpace, Long>(session, SectorOfSpaceTable, SectorOfSpace::id)
