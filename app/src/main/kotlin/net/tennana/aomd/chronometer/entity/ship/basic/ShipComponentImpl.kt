package net.tennana.aomd.chronometer.entity.ship.basic

import net.tennana.aomd.chronometer.entity.ship.generator.PowerSupplyCommand

abstract class ShipComponentImpl(
    private val priority: Int = 0,
    private val mass: Int = 0/*TODO:デフォルト値削除*/,
    protected var capacitorPower: Int = 0
) : ShipComponent {
    protected abstract fun commandPower(): PowerSupplyCommand;

    override fun chargePower(power: Int) {
        capacitorPower += power
    }

    override fun getMass(): Int {
        return mass;
    }

    override fun getPriority(): Int {
        return priority;
    }
}