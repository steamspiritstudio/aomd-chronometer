package net.tennana.aomd.chronometer.entity.ship.generator

import net.tennana.aomd.chronometer.entity.ship.basic.ShipComponent

data class PowerSupplyCommand(val srcComponent: ShipComponent, val required: Int) {
    fun supply(powerSupply: Int) {
        srcComponent.chargePower(powerSupply)
    }
}