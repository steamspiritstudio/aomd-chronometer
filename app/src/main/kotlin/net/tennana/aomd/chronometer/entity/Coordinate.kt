package net.tennana.aomd.chronometer.entity

import net.tennana.aomd.chronometer.logic.flight.DIRECTION

data class Coordinate(val x: Int, val y: Int) {
    fun addDiff(diff: DIRECTION): Coordinate {
        return this.copy(x = x + diff.diffX, y = y + diff.diffY)
    }
}