package net.tennana.aomd.chronometer.log

import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import net.tennana.aomd.chronometer.log.characterAction.CharacterActionBasic
import net.tennana.aomd.chronometer.log.characterAction.CharacterActionResult
import java.time.OffsetDateTime

@Serializable
data class CharacterActionLog(
    val id: String = idGenerator(),
    val basic: CharacterActionBasic,
    val result: CharacterActionResult
) {
    fun toRecord(format: Json): CharacterActionLogRecord {
        val time = OffsetDateTime.now()
        return CharacterActionLogRecord(
            id,
            basic,
            format.encodeToString(result),
            time,
            time
        )
    }
}