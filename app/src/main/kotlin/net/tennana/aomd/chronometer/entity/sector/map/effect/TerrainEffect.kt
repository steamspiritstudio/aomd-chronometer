package net.tennana.aomd.chronometer.entity.sector.map.effect

import net.tennana.aomd.chronometer.entity.sector.map.effect.GasConcentration
import net.tennana.aomd.chronometer.entity.sector.map.effect.Gravitation
import net.tennana.aomd.chronometer.entity.sector.map.effect.RadialRays
import net.tennana.aomd.chronometer.entity.sector.map.effect.SmallBody

data class TerrainEffect(val radialRays: RadialRays = RadialRays(), val gas: GasConcentration = GasConcentration(), val smallBody: SmallBody = SmallBody(), val gravitation: Gravitation = Gravitation()) {
    fun renewWithAdd(other: TerrainEffect): TerrainEffect {
        return this.copy(
            radialRays = radialRays.renewWithAdd(other.radialRays),
            gas = gas.renewWithAdd(other.gas),
            smallBody = smallBody.renewWithAdd(other.smallBody),
            gravitation = gravitation.renewWithAdd(other.gravitation)
        )
    }
}