package net.tennana.aomd.chronometer.log

import net.tennana.aomd.chronometer.log.characterAction.CharacterActionBasic
import java.time.OffsetDateTime

data class CharacterActionLogRecord(
    val id: String,
    val basic: CharacterActionBasic,
    val bodyString: String,
    val createdAt: OffsetDateTime,
    val updatedAt: OffsetDateTime,
) {
}