package net.tennana.aomd.chronometer.log.characterAction

import kotlinx.serialization.Serializable
import net.tennana.aomd.chronometer.entity.CurrentTime
import net.tennana.aomd.chronometer.entity.ship.ShipPlace

@Serializable
data class CharacterActionBasic(
    val characterId: Long,
    val shipId: Long,
    val placeIdInShip: Long,
    val place: ShipPlace,
    val time: CurrentTime
) {
}