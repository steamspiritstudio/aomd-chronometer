package net.tennana.aomd.chronometer.entity.sector

import net.tennana.aomd.chronometer.entity.Coordinate
import net.tennana.aomd.chronometer.entity.ship.ShipPlace

data class Station(
    var id: Long,
    var name: String,
    var sectorId: Long,
    var coordinate: Coordinate,
) {
    fun toShipPlace(): ShipPlace {
        return ShipPlace(sectorId, coordinate.x, coordinate.y, id)
    }
}