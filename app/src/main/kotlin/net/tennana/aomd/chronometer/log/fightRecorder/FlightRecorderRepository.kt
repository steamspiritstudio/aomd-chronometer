package net.tennana.aomd.chronometer.log.fightRecorder

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import net.tennana.aomd.chronometer.log.ShipActionLog
import net.tennana.aomd.chronometer.log.ShipActionResult
import net.tennana.aomd.chronometer.repository.shipActionResultSerializersModule
import net.tennana.aomd.chronometer.table.ShipActionResultDao

class FlightRecorderRepository(val turn: Long, val dao: ShipActionResultDao) {
    fun findAll(): Map<Long, ShipFlightRecorder> {
        val format = Json {
            serializersModule = shipActionResultSerializersModule
        }
        return dao.findByTurn(turn).groupBy { it.basic.shipId }.map { entry ->
            val shipActionLogMap = entry.value.map {
                ShipActionLog(it.id, it.basic, format.decodeFromString<ShipActionResult>(it.bodyString))
            }.groupBy { it.flightBasic.time }
            entry.key to ShipFlightRecorder(entry.key, shipActionLogMap)
        }.toMap()
    }
}