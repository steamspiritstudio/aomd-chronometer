package net.tennana.aomd.chronometer.entity.items

/**
 * アルファ版アイテム: 船の模型
 */
class ShipModel(id: String, basic: BasicData, state: State) : ItemImpl(id, basic, state), CanImproved, ProcessingItem {
    override fun improve(affect: Int) {
        state = state.copy(quality = state.quality + affect)
    }
}