package net.tennana.aomd.chronometer.log

import net.tennana.aomd.chronometer.log.flight.FlightBasic
import net.tennana.aomd.chronometer.logic.flight.affect.ShipAffect

interface ShipActionResult {
    fun convertAffects(flightBasic: FlightBasic): List<ShipAffect>
}