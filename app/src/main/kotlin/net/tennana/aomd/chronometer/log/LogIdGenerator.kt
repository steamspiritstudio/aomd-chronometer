package net.tennana.aomd.chronometer.log

import de.huxhorn.sulky.ulid.ULID

val ulid = ULID()

fun idGenerator(): String {
    return ulid.nextValue().increment().toString()
}