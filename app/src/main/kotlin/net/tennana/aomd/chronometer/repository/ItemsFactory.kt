package net.tennana.aomd.chronometer.repository

import com.github.andrewoma.kwery.core.Row
import com.github.andrewoma.kwery.mapper.Converter
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import net.tennana.aomd.chronometer.entity.CurrentTime
import net.tennana.aomd.chronometer.entity.items.*
import net.tennana.aomd.chronometer.logic.items.ItemIdGenerator
import java.sql.Connection

object ItemsFactory : Converter<Items>(FromDB, ToDB) {
    private val json: Json = Json
    private val csvStore: CsvStore<BasicData> = CsvStore("master/items.csv", BasicData.serializer(), { it.mId })

    object FromDB : (Row, String) -> Items {
        override fun invoke(p1: Row, columnName: String): Items {
            return make(p1.long("id"), p1.string(columnName))
        }
    }

    object ToDB : (Connection, Items) -> Any? {
        override fun invoke(p1: Connection, p2: Items): Any? {
            return json.encodeToString(p2.toDbMap())
        }
    }

    fun make(ownerCharacterId: Long, jsonString: String): Items {
        if (jsonString.length <= 2) {
            return Items(listOf())
        }
        return Items(json.decodeFromString<List<ItemRecord>>(jsonString).map {
            // TODO: クラス分岐
            ShipModel(it.id, csvStore.get(it.mId)!!, State(it.num, it.quality, Owner(ownerCharacterId)))
        })
    }

    fun makeItem(mId: Int, state: State, time: CurrentTime): ItemImpl {
        return ShipModel(ItemIdGenerator.generate(time), csvStore.get(mId), state)
    }
}