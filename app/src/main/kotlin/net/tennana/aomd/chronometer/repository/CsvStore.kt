package net.tennana.aomd.chronometer.repository

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.csv.Csv

@OptIn(ExperimentalSerializationApi::class)
class CsvStore<T>(csvPath: String, serializer: KSerializer<T>, keyMapper: (entity: T) -> Int, delimiter: Char = ',') {
    private val parsedMasterData: Map<Int, T>

    init {
        val csv = Csv {
            hasHeaderRecord = true
            this.delimiter = delimiter
            ignoreUnknownColumns = true
        }
        val file = this.javaClass.classLoader.getResourceAsStream(csvPath)!!.bufferedReader().use {
            it.readText();
        }
        val parsed = csv.decodeFromString(ListSerializer(serializer), file)
        parsedMasterData = parsed.associateBy({ keyMapper(it) }, { it })
    }

    fun get(mId: Int): T {
        return parsedMasterData[mId]!!
    }
}