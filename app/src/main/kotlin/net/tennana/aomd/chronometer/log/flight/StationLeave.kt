package net.tennana.aomd.chronometer.log.flight

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import net.tennana.aomd.chronometer.log.ShipActionResult
import net.tennana.aomd.chronometer.logic.flight.affect.ShipAffect

@Serializable
@SerialName("StationLeave")
class StationLeave() : ShipActionResult {
    override fun convertAffects(flightBasic: FlightBasic): List<ShipAffect> {
        return listOf()
    }
}
