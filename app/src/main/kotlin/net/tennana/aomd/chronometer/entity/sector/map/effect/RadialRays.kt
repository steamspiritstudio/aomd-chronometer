package net.tennana.aomd.chronometer.entity.sector.map.effect

data class RadialRays(val value:Int = 0) {
    fun renewWithAdd(other: RadialRays): RadialRays {
        if(other.value == 0){
            return this;
        }
        return this.copy(value = value + other.value)
    }
}
