package net.tennana.aomd.chronometer.entity.sector

class SectorFairways(private val sectorId:Long, private val fairwayList: List<SectorFairway>) {
    fun toSector(toSectorId: Long): SectorFairway? {
        return fairwayList.find {
            it.wayPointAlpha.sectorId == toSectorId || it.wayPointBeta.sectorId == toSectorId
        }
    }
}