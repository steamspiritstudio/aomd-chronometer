package net.tennana.aomd.chronometer.entity.items

import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
data class State(val hasNum: Int, val quality: Int, @Transient val owner: Owner = Owner(-1)) {
}