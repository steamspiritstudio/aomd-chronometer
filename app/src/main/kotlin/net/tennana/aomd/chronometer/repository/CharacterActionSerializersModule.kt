package net.tennana.aomd.chronometer.repository

import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.modules.polymorphic
import kotlinx.serialization.modules.subclass
import net.tennana.aomd.chronometer.log.characterAction.*

val characterActionResultSerializersModule = SerializersModule {
    polymorphic(CharacterActionResult::class) {
        subclass(ImproveItem::class)
        subclass(Mining::class)
        subclass(Rest::class)
        subclass(SkillStudy::class)
    }
    polymorphic(CharacterActionResult::class) {

    }
}