package net.tennana.aomd.chronometer.entity

import kotlinx.serialization.Serializable

@Serializable
data class CurrentTime(val turn: Long, val time: Int) {
}