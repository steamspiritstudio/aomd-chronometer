package net.tennana.aomd.chronometer.entity.characterAction

import net.tennana.aomd.chronometer.entity.characterAction.basic.ActionRequest
import net.tennana.aomd.chronometer.entity.characterAction.basic.CharacterAction
import net.tennana.aomd.chronometer.entity.characterAction.basic.Time
import net.tennana.aomd.chronometer.log.characterAction.CharacterActionResult
import net.tennana.aomd.chronometer.log.characterAction.Rest as RestLog

class Rest(time: Time) : CharacterAction(time) {
    override fun doAction(request: ActionRequest): Result<CharacterActionResult> {
        // FIXME とりあえず固定値回復
        val affect = RestLog(5);
        val result = request.character.affect(affect);
        if (result.isSuccess) {
            return Result.success(affect)
        }
        return Result.failure(result.exceptionOrNull()!!)
    }
}