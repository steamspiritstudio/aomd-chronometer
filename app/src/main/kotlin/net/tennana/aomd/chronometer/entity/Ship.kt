package net.tennana.aomd.chronometer.entity

import net.tennana.aomd.chronometer.entity.ship.ShipComponents
import net.tennana.aomd.chronometer.entity.ship.ShipPlace
import net.tennana.aomd.chronometer.entity.ship.Visual
import net.tennana.aomd.chronometer.logic.flight.affect.ShipAffect

data class Ship(
    var id: Long,
    val visual: Visual,
    var captainId: Long,
    var place: ShipPlace,
    var tags: String,
    var shelds: Long,
    var money: Long,
    var cargo: String,
    var items: String,
    var createdAt: java.time.LocalDateTime,
    var updatedAt: java.time.LocalDateTime,
) {
    lateinit var components: ShipComponents;

    fun affect(command: List<ShipAffect>) {
        for (shipAffect in command) {
            if (shipAffect is ShipAffect.PlaceAffect) {
                this.affect(shipAffect);
            }
        }
    }

    fun affect(command: ShipAffect.PlaceAffect) {
        place = command.doCommand(place)
    }
}