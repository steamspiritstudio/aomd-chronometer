package net.tennana.aomd.chronometer.table;

import com.github.andrewoma.kwery.core.Session
import com.github.andrewoma.kwery.mapper.AbstractDao
import com.github.andrewoma.kwery.mapper.Table
import com.github.andrewoma.kwery.mapper.Value
import com.github.andrewoma.kwery.mapper.VersionedWithTimestamp
import java.time.LocalDateTime

data class WorldParameter(
    var id: Long,
    var turn: Long,
    var phase: Int,
    var updatedAt: LocalDateTime,
) {}

object WorldTable : Table<WorldParameter, Long>("worlds"), VersionedWithTimestamp {
    val Id by col(WorldParameter::id, id = true)
    val Turn by col(WorldParameter::turn)
    val Phase by col(WorldParameter::phase)
    val UpdatedAt by col(WorldParameter::updatedAt, version = true)
    override fun idColumns(id: Long) = setOf(Id of id)
    override fun create(value: Value<WorldParameter>): WorldParameter {
        return WorldParameter(
            value of Id,
            value of Turn,
            value of Phase,
            value of UpdatedAt,
        )
    }
}

class WorldParameterDao(session: Session) :
    AbstractDao<WorldParameter, Long>(session, WorldTable, WorldParameter::id)
