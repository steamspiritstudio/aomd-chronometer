package net.tennana.aomd.chronometer.entity.sector.map

import net.tennana.aomd.chronometer.entity.sector.TerrainGenerator

class GridMap(private val max_x: Int, private val max_y: Int, terrainGeneratorList: List<TerrainGenerator>){
    private val map: Array<Array<Terrain>>

    init {
        val interstellar = Terrain()
        this.map = (max_x.downTo(0).map { _ ->
            max_y.downTo(0).map { _ ->
                interstellar
            }.toTypedArray()
        }).toTypedArray()
    }

    fun convertToSearchMap(): SearchMap {
        return SearchMap(max_x, max_y, map)
    }

    fun getTerrain(x: Int, y: Int): Terrain {
        return this.map[x][y]
    }
}