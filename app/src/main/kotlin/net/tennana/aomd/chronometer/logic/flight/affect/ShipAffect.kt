package net.tennana.aomd.chronometer.logic.flight.affect

import net.tennana.aomd.chronometer.entity.ship.ShipPlace

sealed interface ShipAffect {
    interface PlaceAffect : ShipAffect {
        fun doCommand(place: ShipPlace): ShipPlace
    }
}