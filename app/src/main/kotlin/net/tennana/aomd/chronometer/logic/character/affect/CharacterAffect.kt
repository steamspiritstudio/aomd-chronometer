package net.tennana.aomd.chronometer.logic.character.affect

import net.tennana.aomd.chronometer.entity.character.Character

sealed interface CharacterAffect {
    interface HpFluctuate {
        fun doCommand(character: Character): Result<Int>
    }
}