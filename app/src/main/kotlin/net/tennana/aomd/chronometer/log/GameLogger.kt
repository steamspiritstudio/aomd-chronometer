package net.tennana.aomd.chronometer.log

import com.github.andrewoma.kwery.core.DefaultSession
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import net.tennana.aomd.chronometer.log.characterAction.CharacterActionBasic
import net.tennana.aomd.chronometer.log.characterAction.CharacterActionResult
import net.tennana.aomd.chronometer.log.flight.FlightBasic
import net.tennana.aomd.chronometer.repository.characterActionResultSerializersModule
import net.tennana.aomd.chronometer.repository.shipActionResultSerializersModule
import net.tennana.aomd.chronometer.table.CharacterActionLogRecordDao
import net.tennana.aomd.chronometer.table.ShipActionResultDao

object GameLogger {
    private val shipActionLogList: ArrayList<ShipActionLog> = ArrayList<ShipActionLog>()
    private val characterActionLogList: ArrayList<CharacterActionLog> = ArrayList<CharacterActionLog>()

    fun addLog(flightBasic: FlightBasic, result: ShipActionResult) {
        this.shipActionLogList.add(ShipActionLog(flightBasic = flightBasic, result = result))
    }

    fun addLog(basic: CharacterActionBasic, log: CharacterActionResult) {
        this.characterActionLogList.add(CharacterActionLog(basic = basic, result = log))
    }

    fun commit(session: DefaultSession) {
        if (shipActionLogList.size > 0) {
            val shipLoggerFormat = Json {
                serializersModule = shipActionResultSerializersModule
                prettyPrint = true
            }
            val shipDbFormat = Json {
                serializersModule = shipActionResultSerializersModule
                prettyPrint = false
            }
            println(shipLoggerFormat.encodeToString(shipActionLogList))
            val dao = ShipActionResultDao(session)
            shipActionLogList.forEach {
                dao.insert(it.toRecord(shipDbFormat))
            }
        }
        if (characterActionLogList.size > 0) {
            val characterLoggerFormat = Json {
                serializersModule = characterActionResultSerializersModule
                prettyPrint = true
            }
            val characterDbFormat = Json {
                serializersModule = characterActionResultSerializersModule
                prettyPrint = false
            }
            println(characterLoggerFormat.encodeToString(characterActionLogList))
            val dao = CharacterActionLogRecordDao(session)
            characterActionLogList.forEach {
                dao.insert(it.toRecord(characterDbFormat))
            }
        }
    }
}