package net.tennana.aomd.chronometer.entity.ship

import net.tennana.aomd.chronometer.entity.CurrentTime
import net.tennana.aomd.chronometer.entity.ship.engine.Engines
import net.tennana.aomd.chronometer.entity.ship.generator.Generators

data class ShipComponents(
    val generators: Generators,
    val engines: Engines,
) {
    fun powerSupply(time: CurrentTime) {
        // 優先順で電力要求をまとめる
        val powerSupplyList = this.engines.commandPower().sortedBy { it.srcComponent.getPriority() }
        // 優先順に沿って電力供給
        powerSupplyList.forEach { generators.request(it) }
    }

    fun getMass(): Int {
        return generators.getMass() + engines.getMass()
    }
}