package net.tennana.aomd.chronometer.entity.sector.map

import net.tennana.aomd.chronometer.entity.sector.map.effect.TerrainEffect

class Terrain(effect: TerrainEffect = TerrainEffect(), private var terrainFlags: TerrainFlags = TerrainFlags()) {
    var effect: TerrainEffect = effect
        private set
}