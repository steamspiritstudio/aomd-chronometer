package net.tennana.aomd.chronometer.entity.characterAction.basic

import net.tennana.aomd.chronometer.log.characterAction.CharacterActionResult

abstract class CharacterAction(
    val time: Time
) {
    open fun isSpecified(request: ActionRequest): Boolean {
        val toTime = time.formTime + time.useAp
        return time.turn == request.time.turn &&
                toTime > request.time.time &&
                time.formTime <= request.time.time
    }

    abstract fun doAction(request: ActionRequest): Result<CharacterActionResult>
}