package net.tennana.aomd.chronometer.entity.character

data class Health(val currentHp: Int, val maxHp: Int = 100) {
    fun calcAffectHp(hp: Int): Result<Int> {
        // FIXME: 最大HPが固定値
        val result = (currentHp + hp).coerceAtMost(maxHp);
        if (result == hp) {
            return Result.failure(Exception("これ以上は回復しない"));
        }
        return Result.success(result);
    }
}