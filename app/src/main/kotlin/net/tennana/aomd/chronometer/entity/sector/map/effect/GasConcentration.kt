package net.tennana.aomd.chronometer.entity.sector.map.effect

data class GasConcentration(val value:Int = 0) {
    fun renewWithAdd(other: GasConcentration): GasConcentration {
        if(other.value == 0){
            return this;
        }
        return this.copy(value = value + other.value)
    }
}
