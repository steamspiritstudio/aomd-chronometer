package net.tennana.aomd.chronometer.entity.characterAction.basic

import net.tennana.aomd.chronometer.entity.characterAction.Rest

data class CharacterActions(val characterId: Long, val actions: List<CharacterAction>) {
    fun popAction(request: ActionRequest): CharacterAction {
        // TODO: 実行できるアクションがないときはデフォルトアクションを行うが、その旨をログに残すようにしたい
        return actions.find { it.isSpecified(request) } ?: Rest(Time(request.time.turn, 1, request.time.time.toLong()))
    }
}