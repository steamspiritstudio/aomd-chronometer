package net.tennana.aomd.chronometer.entity.ship.basic

import net.tennana.aomd.chronometer.entity.ship.generator.PowerSupplyCommand

interface RequirePowerSupply {
    fun commandPower() : List<PowerSupplyCommand>
}