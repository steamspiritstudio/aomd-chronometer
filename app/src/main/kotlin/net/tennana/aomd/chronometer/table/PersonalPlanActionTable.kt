package net.tennana.aomd.chronometer.table;

import com.github.andrewoma.kwery.core.Session
import com.github.andrewoma.kwery.mapper.AbstractDao
import com.github.andrewoma.kwery.mapper.Table
import com.github.andrewoma.kwery.mapper.Value
import com.github.andrewoma.kwery.mapper.VersionedWithTimestamp

data class PersonalPlanActionId(val characterId: Long, val turn: Long, val order: Long) {}

data class PersonalPlanAction(
    val id: PersonalPlanActionId,
    var characterId: Long,
    var turn: Long,
    var order: Long,
    var useAp: Long,
    var fromTime: Long,
    var actionType: String,
    var targetId: Long,
    var paramJson: String,
    var createdAt: java.time.LocalDateTime,
    var updatedAt: java.time.LocalDateTime,
) {}

object PersonalPlanActionTable : Table<PersonalPlanAction, PersonalPlanActionId>("personal_plan_actions"),
    VersionedWithTimestamp {
    val CharacterId by col(PersonalPlanAction::characterId)
    val Turn by col(PersonalPlanAction::turn)
    val Order by col(PersonalPlanAction::order)
    val UseAp by col(PersonalPlanAction::useAp)
    val FromTime by col(PersonalPlanAction::fromTime)
    val ActionType by col(PersonalPlanAction::actionType)
    val TargetId by col(PersonalPlanAction::targetId)
    val ParamJson by optionalCol(PersonalPlanAction::paramJson, { it })
    val CreatedAt by col(PersonalPlanAction::createdAt)
    val UpdatedAt by col(PersonalPlanAction::updatedAt, version = true)
    override fun idColumns(id: PersonalPlanActionId) =
        setOf(CharacterId of id.characterId, Turn of id.turn, Order of id.order)

    override fun create(value: Value<PersonalPlanAction>): PersonalPlanAction {
        return PersonalPlanAction(
            PersonalPlanActionId(value of CharacterId, value of Turn, value of Order),
            value of CharacterId,
            value of Turn,
            value of Order,
            value of UseAp,
            value of FromTime,
            value of ActionType,
            value of TargetId,
            (value of ParamJson) ?: "",
            value of CreatedAt,
            value of UpdatedAt,
        )
    }
}

class PersonalPlanActionDao(session: Session) :
    AbstractDao<PersonalPlanAction, PersonalPlanActionId>(session, PersonalPlanActionTable, PersonalPlanAction::id) {
    fun findByTurn(turn: Long): List<PersonalPlanAction> {
        val name = "findByTurn"

        val sql = sql(Pair(name, turn)) {
            "select * \nfrom ${table.name}\nwhere turn = :turn"
        }
        return session.select(sql, mapOf(Pair("turn", turn)), options(name), table.rowMapper(table.allColumns))
    }
}
