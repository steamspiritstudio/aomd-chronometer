package net.tennana.aomd.chronometer.entity.characterAction.basic

data class Time(
    val turn: Long,
    val useAp: Long,
    val formTime: Long
) {
}