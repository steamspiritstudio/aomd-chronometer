package net.tennana.aomd.chronometer.log

import net.tennana.aomd.chronometer.log.flight.FlightBasic
import net.tennana.aomd.chronometer.logic.flight.affect.ShipAffect

/**
 * ログ出力がまだ実装されていない箇所のログ
 * @deprecated
 */
class TodoImplementLog : ShipActionResult {
    override fun convertAffects(flightBasic: FlightBasic): List<ShipAffect> {
        throw Exception("未実装ログから影響コマンドを取り出そうとした")
    }
}