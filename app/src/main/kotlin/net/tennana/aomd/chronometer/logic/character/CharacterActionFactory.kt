package net.tennana.aomd.chronometer.logic.character

import net.tennana.aomd.chronometer.entity.characterAction.*
import net.tennana.aomd.chronometer.entity.characterAction.basic.CharacterAction
import net.tennana.aomd.chronometer.entity.characterAction.basic.Time
import net.tennana.aomd.chronometer.table.PersonalPlanAction

class CharacterActionFactory {
    public fun make(record: PersonalPlanAction): CharacterAction {
        val time = Time(record.turn, record.useAp, record.fromTime)
        return when (record.actionType) {
            "mining" -> Mining(time)
            "empty" -> Empty(time)
            "rest" -> Rest(time)
            "study" -> SkillStudy(time)
            "assembly" -> AssemblyModel(time)
            else -> {
                println("不正なレコード:" + record.id.toString());
                Empty(time)
            }
        }
    }
}