package net.tennana.aomd.chronometer.entity.ship

import net.tennana.aomd.chronometer.entity.Coordinate
import net.tennana.aomd.chronometer.entity.CurrentTime
import net.tennana.aomd.chronometer.entity.Ship
import net.tennana.aomd.chronometer.entity.Universe
import net.tennana.aomd.chronometer.log.ShipActionResult
import net.tennana.aomd.chronometer.log.flight.MovedSector
import net.tennana.aomd.chronometer.log.flight.MovedShip
import net.tennana.aomd.chronometer.log.flight.StationArrive
import java.time.LocalDateTime

sealed class ShipFlightScheduleRecord(
    val id: Long,
    val shipId: Long,
    var waitAp: Long,
    open val toSectorId: Long?,
    open val toStationId: Long?,
    open val coordinateX: Long?,
    open val coordinateY: Long?,
    val registerAccountId: Long,
    val registerCharacterId: Long,
    val createdAt: java.time.LocalDateTime,
    val updatedAt: java.time.LocalDateTime,
) {
    abstract fun toCoordinate(universe: Universe, currentSectorId: Long): Coordinate
    abstract fun doArrive(universe: Universe, time: CurrentTime, ship: Ship): Result<ShipActionResult>
}

class StationApproach(
    id: Long,
    shipId: Long,
    waitAp: Long,
    override val toStationId: Long,
    registerAccountId: Long,
    registerCharacterId: Long,
    createdAt: LocalDateTime,
    updatedAt: LocalDateTime
) : ShipFlightScheduleRecord(
    id,
    shipId,
    waitAp,
    null,
    toStationId,
    null,
    null,
    registerAccountId,
    registerCharacterId,
    createdAt,
    updatedAt
) {
    override fun toCoordinate(universe: Universe, currentSectorId: Long): Coordinate {
        val station = universe.findStation(toStationId)
        if (station.sectorId == currentSectorId) {
            return station.coordinate
        }
        val toSectorId = station.sectorId
        val currentSector = universe.findSector(currentSectorId)
        val fairway = currentSector.fairways.toSector(toSectorId)
            ?: // 探査できないセクターの場合、現在位置に留まる
            return universe.findShip(shipId).place.toCoordinate()
        return fairway.getWayPointFrom(currentSectorId).toCoordinate()
    }

    override fun doArrive(universe: Universe, time: CurrentTime, ship: Ship): Result<ShipActionResult> {
        val station = universe.findStation(toStationId)
        if (ship.place.sectorId != station.sectorId) {
            return Result.failure(Exception("目的地未到達(セクターが異なる)"))
        }
        if (ship.place.toCoordinate() == station.coordinate) {
            ship.place = ship.place.stationArrive(station.id)
            return Result.success(StationArrive(station))
        }
        return Result.failure(Exception("目的地未到達(セクター内)"))
    }
}

class MoveSector(
    id: Long,
    shipId: Long,
    waitAp: Long,
    override val toSectorId: Long,
    val coordinate: Coordinate,
    registerAccountId: Long,
    registerCharacterId: Long,
    createdAt: LocalDateTime,
    updatedAt: LocalDateTime
) : ShipFlightScheduleRecord(
    id,
    shipId,
    waitAp,
    toSectorId,
    null,
    coordinate.x.toLong(),
    coordinate.y.toLong(),
    registerAccountId,
    registerCharacterId,
    createdAt,
    updatedAt
) {
    override fun toCoordinate(universe: Universe, currentSectorId: Long): Coordinate {
        if (currentSectorId == toSectorId) {
            return coordinate
        }
        val currentSector = universe.findSector(currentSectorId)
        // TODO: 隣接セクターしか探索していないので、必要が出てくればセクター間探索を実装する
        val fairway = currentSector.fairways.toSector(toSectorId)
            ?: // 探査できないセクターの場合、現在位置に留まる
            return universe.findShip(shipId).place.toCoordinate()
        return fairway.getWayPointFrom(currentSectorId).toCoordinate()
    }

    override fun doArrive(universe: Universe, time: CurrentTime, ship: Ship): Result<ShipActionResult> {
        if (ship.place.sectorId == toSectorId) {
            if (ship.place.toCoordinate().equals(coordinate)) {
                return Result.success(MovedShip(ship.place))
            }
            return Result.failure(Exception("指定座礁に到達していない"))
        }
        val currentSector = universe.findSector(ship.place.sectorId)
        // TODO: 隣接セクターしか探索していないので、必要が出てくればセクター間探索を実装する
        val fairway = currentSector.fairways.toSector(toSectorId)
        if (fairway == null) {
            return Result.failure(Exception("航路が発見できない移動先"))
        }
        val waypoint = fairway.getWayPointFrom(ship.place.sectorId);
        if (!waypoint.inRange(ship.place)) {
            return Result.failure(Exception("航路帯の外"))
        }
        val toSector = universe.findSector(toSectorId)
        val srcPlace = ship.place
        val diffX = ship.place.x - srcPlace.x;
        val diffY = ship.place.y - srcPlace.y;
        val toWayPoint = fairway.getWayPointTo(toSectorId)
        ship.place =
            ship.place.copy(sectorId = toSectorId, x = toWayPoint.x.toInt() + diffX, y = toWayPoint.y.toInt() + diffY)
        return Result.success(MovedSector(ship.place))
    }
}