package net.tennana.aomd.chronometer.repository

import com.github.andrewoma.kwery.core.Session
import net.tennana.aomd.chronometer.table.WorldParameter
import net.tennana.aomd.chronometer.table.WorldParameterDao

enum class PHASE(val number: Int) {
    SHIP(0),
    CHARACTER(1)
}

object WorldParameters {
    private lateinit var allData: WorldParameter
    private lateinit var dao: WorldParameterDao
    private var currentPhase: Int = 0

    fun setup(session: Session) {
        this.dao = WorldParameterDao(session);
        this.allData = this.dao.findById(1)!!
        this.currentPhase = this.allData.phase
    }

    fun getTurn(): Long {
        return this.allData.turn
    }

    fun next() {
        if (this.isCharacterPhase()) {
            this.allData.turn++;
            this.allData.phase = 0;
            return
        }
        this.allData.phase++;
    }

    fun isShipPhase(): Boolean {
        return this.currentPhase == PHASE.SHIP.number;
    }

    fun isCharacterPhase(): Boolean {
        return this.currentPhase == PHASE.CHARACTER.number;
    }

    fun commit() {
        this.dao.unsafeUpdate(this.allData)
    }

    fun getPhaseName(): String {
        if (this.allData.phase == PHASE.SHIP.number) {
            return "船"
        }
        return "キャラ";
    }
}