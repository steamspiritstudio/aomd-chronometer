package net.tennana.aomd.chronometer.entity.characterAction

import net.tennana.aomd.chronometer.entity.characterAction.basic.ActionRequest
import net.tennana.aomd.chronometer.entity.characterAction.basic.CharacterAction
import net.tennana.aomd.chronometer.entity.characterAction.basic.Time
import net.tennana.aomd.chronometer.entity.items.Owner
import net.tennana.aomd.chronometer.entity.items.State
import net.tennana.aomd.chronometer.log.characterAction.CharacterActionResult
import net.tennana.aomd.chronometer.log.characterAction.Mining
import net.tennana.aomd.chronometer.repository.ItemsFactory

class Mining(time: Time) : CharacterAction(time) {
    override fun doAction(request: ActionRequest): Result<CharacterActionResult> {
        val terrainEffect = request.universe.findTerrain(request.ship.place).effect
        if (terrainEffect.smallBody.value <= 0) {
            return Result.failure(Exception("採掘対象の小惑星がない"))
        }
        val gotItem =
            ItemsFactory.makeItem(2, State(terrainEffect.smallBody.value, 0, Owner(request.character.id)), request.time)
        return Result.success(Mining(true, gotItem.basic.mId, gotItem.state))
    }
}