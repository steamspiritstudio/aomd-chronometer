package net.tennana.aomd.chronometer.log.flight

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import net.tennana.aomd.chronometer.entity.sector.Station
import net.tennana.aomd.chronometer.entity.ship.ShipPlace
import net.tennana.aomd.chronometer.log.ShipActionResult
import net.tennana.aomd.chronometer.logic.flight.affect.ReplacePlaceAffect
import net.tennana.aomd.chronometer.logic.flight.affect.ShipAffect

@Serializable
@SerialName("StationArrive")
class StationArrive(val place: ShipPlace, val name: String) : ShipActionResult {
    constructor(
        station: Station
    ) : this(station.toShipPlace(), station.name)

    override fun convertAffects(flightBasic: FlightBasic): List<ShipAffect> {
        return listOf(ReplacePlaceAffect(place))
    }
}
