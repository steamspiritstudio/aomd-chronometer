package net.tennana.aomd.chronometer.entity.character

data class Visual(
    val callsign: String,
    val name: String,
    val registerTurn: Long,
    val portrait: String,
    val personality: String,
    val iconIds: String
) {
}