package net.tennana.aomd.chronometer.entity.ship.engine

import net.tennana.aomd.chronometer.entity.CurrentTime
import net.tennana.aomd.chronometer.entity.ship.basic.RequirePowerSupply
import net.tennana.aomd.chronometer.entity.ship.basic.VoyageComponent
import net.tennana.aomd.chronometer.entity.ship.generator.PowerSupplyCommand

class Engines(private val engineList: List<Engine>) : RequirePowerSupply, VoyageComponent {
    fun doCommand(time: CurrentTime): DrivingForce {
        // 推進力へ変換して返す
        return engineList.fold(DrivingForce(0.0)) { sum, engine -> engine.doCommand(time).renewWithAdd(sum) };
    }

    override fun commandPower(): List<PowerSupplyCommand> {
        return engineList.map { it.commandPower() }
    }

    fun getMass(): Int {
        return this.engineList.fold(0) { sum, engine -> sum + engine.getMass() }
    }
}