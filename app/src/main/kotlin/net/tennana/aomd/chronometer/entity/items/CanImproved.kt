package net.tennana.aomd.chronometer.entity.items

interface CanImproved {
    fun improve(affect: Int)
}