package net.tennana.aomd.chronometer.repository

import com.github.andrewoma.kwery.core.Session
import net.tennana.aomd.chronometer.entity.character.Character
import net.tennana.aomd.chronometer.table.CharacterDao

object CharacterRepository {
    private lateinit var all: List<Character>
    private lateinit var dao: CharacterDao

    fun setup(session: Session) {
        this.dao = CharacterDao(session)
        this.all = this.dao.findAll();
    }

    fun all(): List<Character> {
        return this.all;
    }

    fun findById(characterId: Long): Character {
        return this.all.find { character: Character -> character.id == characterId }!!
    }

    fun commit() {
        this.all.forEach { character -> this.dao.unsafeUpdate(character) }
    }
}