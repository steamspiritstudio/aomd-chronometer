package net.tennana.aomd.chronometer.entity.character

import net.tennana.aomd.chronometer.entity.items.Items
import net.tennana.aomd.chronometer.logic.character.affect.CharacterAffect

data class Character(
    val id: Long,
    val accountId: Long,
    val visual: Visual,
    var placeIdInShip: Long,
    var health: Health,
    var sp: Long,
    var tags: String,
    var money: Long,
    val items: Items,
    var createdAt: java.time.LocalDateTime,
    var updatedAt: java.time.LocalDateTime,
) {

    fun affect(affect: CharacterAffect.HpFluctuate): Result<CharacterAffect.HpFluctuate> {
        val result = affect.doCommand(this);
        health = health.copy(currentHp = result.getOrElse { health.currentHp }.toInt())
        if (result.isSuccess) {
            return Result.success(affect)
        }
        return Result.failure(result.exceptionOrNull()!!);
    }

    val shipId = id;
}