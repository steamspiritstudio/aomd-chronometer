package net.tennana.aomd.chronometer.table;
import com.github.andrewoma.kwery.core.Session
import com.github.andrewoma.kwery.mapper.AbstractDao
import com.github.andrewoma.kwery.mapper.Table
import com.github.andrewoma.kwery.mapper.Value
import com.github.andrewoma.kwery.mapper.VersionedWithTimestamp

data class SectorObject (
  var id: Long,
  var name: String,
  var type: Long,
  var discoveryName: String,
  var sectorId: Long,
  var centralCoordinateX: Long,
  var centralCoordinateY: Long,
){}
object SectorObjectTable : Table<SectorObject, Long>("sector_object") {
   val Id by col(SectorObject::id, id = true)
   val Name by col(SectorObject::name)
   val Type by col(SectorObject::type)
   val DiscoveryName by col(SectorObject::discoveryName)
   val SectorId by col(SectorObject::sectorId)
   val CentralCoordinateX by col(SectorObject::centralCoordinateX)
   val CentralCoordinateY by col(SectorObject::centralCoordinateY)
    override fun idColumns(id: Long) = setOf(Id of id)
    override fun create(value: Value<SectorObject>): SectorObject {
                     return SectorObject(
   value of Id,
   value of Name,
   value of Type,
   value of DiscoveryName,
   value of SectorId,
   value of CentralCoordinateX,
   value of CentralCoordinateY,
)}
}

class SectorObjectDao(session: Session) : AbstractDao<SectorObject, Long>(session, SectorObjectTable, SectorObject::id)
