package net.tennana.aomd.chronometer.logic.flight

import com.github.andrewoma.kwery.core.Session
import net.tennana.aomd.chronometer.entity.ship.ShipFlightScheduleRecord
import net.tennana.aomd.chronometer.table.ShipFlightScheduleDao

class ShipFlightScheduleRepository(
    private var dao: ShipFlightScheduleDao,
    private var schedule: Map<Long, ArrayList<ShipFlightScheduleRecord>>
) {
    private val commandedId = hashSetOf<Long>()
    private val updatedRecord = hashMapOf<Long, ShipFlightScheduleRecord>()

    companion object {
        fun setup(
            session: Session
        ): ShipFlightScheduleRepository {
            val dao = ShipFlightScheduleDao(session);
            return ShipFlightScheduleRepository(
                dao,
                dao.findAll().groupBy { it.shipId }.mapValues { ArrayList(it.value) }
            )
        }
    }

    fun isEmpty(shipId: Long): Boolean {
        return schedule[shipId].isNullOrEmpty()
    }

    fun nextPlan(shipId: Long): ShipFlightScheduleRecord {
        return schedule[shipId]!!.first()
    }

    fun decrementWaitAp(record: ShipFlightScheduleRecord): Boolean {
        record.waitAp--
        if (record.waitAp <= 0) {
            completePlan(record)
            return true
        }
        updatedRecord[record.id] = record
        return false
    }

    private fun completePlan(record: ShipFlightScheduleRecord) {
        schedule[record.shipId]!!.remove(record)
        commandedId.add(record.id)
        updatedRecord.remove(record.id)
    }

    fun commit() {
        commandedId.forEach {
            dao.delete(it)
        }
        if (updatedRecord.values.isEmpty()) {
            return
        }
        dao.unsafeBatchUpdate(updatedRecord.values.toMutableList())
    }
}