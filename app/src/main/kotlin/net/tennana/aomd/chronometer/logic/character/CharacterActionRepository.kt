package net.tennana.aomd.chronometer.logic.character

import net.tennana.aomd.chronometer.entity.characterAction.basic.CharacterActions
import net.tennana.aomd.chronometer.table.PersonalPlanActionDao

class CharacterActionRepository(
    val dao: PersonalPlanActionDao,
    val factory: CharacterActionFactory,
    val turn: Long
) {
    lateinit var all: List<CharacterActions>

    fun setup() {
        this.all = dao.findByTurn(turn)
            .groupBy { it.characterId }
            .map {
                CharacterActions(it.key, it.value.sortedBy { record -> record.order }.map { record ->
                    factory.make(record)
                })
            }
    }

    fun findById(characterId: Long): CharacterActions {
        return all.find { it.characterId == characterId } ?: return CharacterActions(characterId, listOf())
    }
}