package net.tennana.aomd.chronometer.table;

import com.github.andrewoma.kwery.core.Session
import com.github.andrewoma.kwery.mapper.*
import net.tennana.aomd.chronometer.entity.character.Character
import net.tennana.aomd.chronometer.entity.character.Health
import net.tennana.aomd.chronometer.entity.character.Visual
import net.tennana.aomd.chronometer.entity.items.Items
import net.tennana.aomd.chronometer.repository.ItemsFactory
import java.time.LocalDateTime

object CharacterTable : Table<Character, Long>("characters"), VersionedWithTimestamp {
    val Id by col(Character::id, id = true)
    val AccountId by col(Character::accountId)
    val Callsign by col(Visual::callsign, Character::visual)
    val Name by col(Visual::name, Character::visual)
    val RegisterTurn by col(Visual::registerTurn, Character::visual)
    val Portrait by optionalCol(Visual::portrait, Character::visual)
    val Personality by col(Visual::personality, Character::visual)
    val IconIds by col(Visual::iconIds, Character::visual)
    val PlaceIdInShip by col(Character::placeIdInShip)
    val Helth by col(Health::currentHp, Character::health)
    val Sp by col(Character::sp)
    val Tags by col(Character::tags)
    val Money by col(Character::money)
    val Items: Column<Character, Items> by col(Character::items, converter = ItemsFactory, default = Items(listOf()))
    val CreatedAt by col(Character::createdAt, default = LocalDateTime.now())
    val UpdatedAt by col(Character::updatedAt, version = true)
    override fun idColumns(id: Long) = setOf(Id of id)
    override fun create(value: Value<Character>): Character {
        return Character(
            value of Id,
            value of AccountId,
            Visual(
                value of Callsign,
                value of Name,
                value of RegisterTurn,
                (value of Portrait).orEmpty(),
                value of Personality,
                value of IconIds
            ),
            value of PlaceIdInShip,
            Health(value of Helth),
            value of Sp,
            value of Tags,
            value of Money,
            value of Items,
            value of CreatedAt,
            value of UpdatedAt,
        )
    }
}

class CharacterDao(session: Session) : AbstractDao<Character, Long>(session, CharacterTable, Character::id)
