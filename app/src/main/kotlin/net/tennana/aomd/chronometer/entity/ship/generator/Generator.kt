package net.tennana.aomd.chronometer.entity.ship.generator

import net.tennana.aomd.chronometer.entity.CurrentTime
import net.tennana.aomd.chronometer.entity.ship.ComponentDoCommandResult
import net.tennana.aomd.chronometer.entity.ship.basic.ShipComponentImpl
import net.tennana.aomd.chronometer.entity.ship.basic.VoyageComponent

data class Generator(val supply: Int) : ShipComponentImpl(), VoyageComponent {
    override fun commandPower(): PowerSupplyCommand {
        throw RuntimeException("ジェネレーターは電力要求を行わない")
    }

    override fun doCommand(time: CurrentTime): ComponentDoCommandResult {
        throw RuntimeException("ジェネレーターは個別処理を行わない")
    }
}

