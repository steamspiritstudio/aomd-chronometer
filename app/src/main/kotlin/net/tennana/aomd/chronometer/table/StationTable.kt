package net.tennana.aomd.chronometer.table;

import com.github.andrewoma.kwery.core.Session
import com.github.andrewoma.kwery.mapper.AbstractDao
import com.github.andrewoma.kwery.mapper.Table
import com.github.andrewoma.kwery.mapper.Value
import com.github.andrewoma.kwery.mapper.VersionedWithTimestamp
import net.tennana.aomd.chronometer.entity.Coordinate
import net.tennana.aomd.chronometer.entity.sector.Station

object StationTable : Table<Station, Long>("stations") {
    val Id by col(Station::id, id = true)
    val Name by col(Station::name)
    val SectorId by col(Station::sectorId)
    val CoordinateX by col(Coordinate::x, Station::coordinate)
    val CoordinateY by col(Coordinate::y, Station::coordinate)
    override fun idColumns(id: Long) = setOf(Id of id)
    override fun create(value: Value<Station>): Station {
        return Station(
            value of Id,
            value of Name,
            value of SectorId,
            Coordinate(
                value of CoordinateX,
                value of CoordinateY
            ),
        )
    }
}

class StationDao(session: Session) : AbstractDao<Station, Long>(session, StationTable, Station::id)
