package net.tennana.aomd.chronometer.table;

import com.github.andrewoma.kwery.core.Session
import com.github.andrewoma.kwery.mapper.AbstractDao
import com.github.andrewoma.kwery.mapper.Table
import com.github.andrewoma.kwery.mapper.Value
import net.tennana.aomd.chronometer.entity.CurrentTime
import net.tennana.aomd.chronometer.entity.ship.ShipPlace
import net.tennana.aomd.chronometer.log.CharacterActionLogRecord
import net.tennana.aomd.chronometer.log.characterAction.CharacterActionBasic

object CharacterActionLogRecordTable : Table<CharacterActionLogRecord, String>("character_action_results") {
    val Id by col(CharacterActionLogRecord::id, id = true)
    val Turn by col(CurrentTime::turn, path = fun(record) = record.basic.time)
    val Time by col(CurrentTime::time, path = fun(record) = record.basic.time)
    val CharacterId by col(CharacterActionBasic::characterId, CharacterActionLogRecord::basic)
    val ShipId by col(CharacterActionBasic::shipId, CharacterActionLogRecord::basic)
    val SectorId by col(ShipPlace::sectorId, fun(record) = record.basic.place)
    val ShipX by col(ShipPlace::x, fun(record) = record.basic.place)
    val ShipY by col(ShipPlace::y, fun(record) = record.basic.place)
    val StationId by col(ShipPlace::stationId, fun(record) = record.basic.place)
    val BodyJson by col(CharacterActionLogRecord::bodyString)
    val CreatedAt by col(CharacterActionLogRecord::createdAt)
    val UpdatedAt by col(CharacterActionLogRecord::updatedAt)
    override fun idColumns(id: String) = setOf(Id of id)
    override fun create(value: Value<CharacterActionLogRecord>): CharacterActionLogRecord {
        return CharacterActionLogRecord(
            value of Id,
            CharacterActionBasic(
                value of CharacterId,
                value of ShipId,
                -1,
                ShipPlace(
                    value of SectorId,
                    value of ShipX,
                    value of ShipY,
                    value of StationId,
                ),
                CurrentTime(
                    value of Turn,
                    value of Time
                ),
            ),
            value of BodyJson,
            value of CreatedAt,
            value of UpdatedAt,
        )
    }
}

class CharacterActionLogRecordDao(session: Session) :
    AbstractDao<CharacterActionLogRecord, String>(session, CharacterActionLogRecordTable, CharacterActionLogRecord::id)
