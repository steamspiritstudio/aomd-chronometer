package net.tennana.aomd.chronometer.entity.characterAction

import net.tennana.aomd.chronometer.entity.characterAction.basic.ActionRequest
import net.tennana.aomd.chronometer.entity.characterAction.basic.CharacterAction
import net.tennana.aomd.chronometer.entity.characterAction.basic.Time
import net.tennana.aomd.chronometer.entity.items.CanImproved
import net.tennana.aomd.chronometer.entity.items.Owner
import net.tennana.aomd.chronometer.entity.items.State
import net.tennana.aomd.chronometer.log.characterAction.CharacterActionResult
import net.tennana.aomd.chronometer.log.characterAction.ImproveItem
import net.tennana.aomd.chronometer.repository.ItemsFactory

class AssemblyModel(time: Time) : CharacterAction(time) {
    override fun doAction(request: ActionRequest): Result<CharacterActionResult> {
        // 模型
        val item = request.character.items.findOne(1) ?: run {
            val newItem = ItemsFactory.makeItem(1, State(1, 0, Owner(request.character.id)), request.time);
            request.character.items.addItem(newItem)
            return@run newItem
        }
        if (item !is CanImproved) {
            return Result.failure(Exception("不正なアイテムが生成されました"))
        }
        // TODO: スキルに影響させる
        val beforeState = item.state.copy()
        val affect = 5;
        item.improve(affect);

        return Result.success(ImproveItem.make(item, beforeState))
    }
}