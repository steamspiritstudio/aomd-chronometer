package net.tennana.aomd.chronometer.log.characterAction

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import net.tennana.aomd.chronometer.entity.items.CanImproved
import net.tennana.aomd.chronometer.entity.items.ItemImpl
import net.tennana.aomd.chronometer.entity.items.State

@Serializable
@SerialName("ImproveItem")
class ImproveItem(
    val itemId: String,
    val mItemId: Int,
    val beforeQuality: Int,
    val afterQuality: Int,
) : CharacterActionResult {
    companion object {
        fun make(item: CanImproved, beforeState: State): ImproveItem {
            if (item !is ItemImpl) {
                throw Exception()
            }
            return ImproveItem(item.id, item.basic.mId, beforeState.quality, item.state.quality)
        }
    }
}