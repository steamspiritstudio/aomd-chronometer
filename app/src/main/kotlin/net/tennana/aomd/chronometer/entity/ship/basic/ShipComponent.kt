package net.tennana.aomd.chronometer.entity.ship.basic

import net.tennana.aomd.chronometer.entity.CurrentTime
import net.tennana.aomd.chronometer.entity.ship.ComponentDoCommandResult

interface ShipComponent {
    fun doCommand(time: CurrentTime): ComponentDoCommandResult
    fun chargePower(power: Int)
    fun getMass(): Int;
    fun getPriority(): Int;
}