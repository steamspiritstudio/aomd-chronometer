package net.tennana.aomd.chronometer.entity.characterAction

import net.tennana.aomd.chronometer.entity.characterAction.basic.ActionRequest
import net.tennana.aomd.chronometer.entity.characterAction.basic.CharacterAction
import net.tennana.aomd.chronometer.entity.characterAction.basic.Time
import net.tennana.aomd.chronometer.log.characterAction.CharacterActionResult

class SkillStudy(time: Time) : CharacterAction(time) {
    override fun doAction(request: ActionRequest): Result<CharacterActionResult> {
        return Result.failure(Exception("実装されていないので失敗"))
    }
}