package net.tennana.aomd.chronometer.table;

import com.github.andrewoma.kwery.core.Session
import com.github.andrewoma.kwery.mapper.AbstractDao
import com.github.andrewoma.kwery.mapper.Table
import com.github.andrewoma.kwery.mapper.Value
import com.github.andrewoma.kwery.mapper.VersionedWithTimestamp
import net.tennana.aomd.chronometer.entity.Ship
import net.tennana.aomd.chronometer.entity.ship.ShipPlace
import net.tennana.aomd.chronometer.entity.ship.Visual

object ShipTable : Table<Ship, Long>("ships"), VersionedWithTimestamp {
    val Id by col(Ship::id, id = true)
    val Callsign by col(Visual::callsign, Ship::visual)
    val Name by col(Visual::name, Ship::visual)
    val CaptainId by col(Ship::captainId)
    val CoordinateX by col(ShipPlace::x, Ship::place)
    val CoordinateY by col(ShipPlace::y, Ship::place)
    val PlaceSectorId by col(ShipPlace::sectorId, Ship::place)
    val PlaceStationId by col(ShipPlace::stationId, Ship::place)
    val Tags by col(Ship::tags)
    val Shelds by col(Ship::shelds)
    val Money by col(Ship::money)
    val Cargo by col(Ship::cargo)
    val Items by col(Ship::items)
    val FreeTxt by col(Visual::freeTxt, Ship::visual)
    val Picture by col(Visual::picture, Ship::visual)
    val CreatedAt by col(Ship::createdAt)
    val UpdatedAt by col(Ship::updatedAt, version = true)
    override fun idColumns(id: Long) = setOf(Id of id)
    override fun create(value: Value<Ship>): Ship {
        return Ship(
            value of Id,
            Visual(
                value of Callsign,
                value of Name,
                value of FreeTxt,
                value of Picture,
            ),
            value of CaptainId,
            ShipPlace(
                value of PlaceSectorId,
                value of CoordinateX,
                value of CoordinateY,
                value of PlaceStationId,
            ),
            value of Tags,
            value of Shelds,
            value of Money,
            value of Cargo,
            value of Items,
            value of CreatedAt,
            value of UpdatedAt,
        )
    }
}

class ShipDao(session: Session) : AbstractDao<Ship, Long>(session, ShipTable, Ship::id)
