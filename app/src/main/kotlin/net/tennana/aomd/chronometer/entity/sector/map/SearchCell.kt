package net.tennana.aomd.chronometer.entity.sector.map

import net.tennana.aomd.chronometer.entity.Coordinate

data class SearchCell(val coordinate: Coordinate,val terrain: Terrain, var searchPoint: Long = Long.MAX_VALUE) {
    companion object {
        fun makeUnmovableCell(coordinate: Coordinate): SearchCell {
            return SearchCell(coordinate, Terrain(), Long.MAX_VALUE - 1)
        }
    }
    fun active(): Boolean {
        return searchPoint < Long.MAX_VALUE
    }
}