package net.tennana.aomd.chronometer.entity.ship

data class Visual(
    val callsign: String,
    val name: String,
    val freeTxt: String?,
    val picture: String?
) {
}