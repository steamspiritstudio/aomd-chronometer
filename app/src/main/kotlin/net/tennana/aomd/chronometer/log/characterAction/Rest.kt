package net.tennana.aomd.chronometer.log.characterAction

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import net.tennana.aomd.chronometer.entity.character.Character
import net.tennana.aomd.chronometer.logic.character.affect.CharacterAffect

@Serializable
@SerialName("Rest")
data class Rest(val hp: Int, var affectHp: Int = 0) : CharacterActionResult, CharacterAffect.HpFluctuate {
    override fun doCommand(character: Character): Result<Int> {
        val before = character.health.currentHp;
        val result = character.health.calcAffectHp(hp)
        if (result.isSuccess) {
            affectHp = before - result.getOrThrow();
        }
        return result
    }
}