package net.tennana.aomd.chronometer.logic.flight.affect

import net.tennana.aomd.chronometer.entity.ship.ShipPlace

data class ReplacePlaceAffect(val shipPlace: ShipPlace) : ShipAffect.PlaceAffect {
    override fun doCommand(place: ShipPlace): ShipPlace {
        return shipPlace
    }
}