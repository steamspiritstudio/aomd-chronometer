package net.tennana.aomd.chronometer.entity.sector.map

import net.tennana.aomd.chronometer.entity.Coordinate
import net.tennana.aomd.chronometer.entity.sector.TerrainGenerator

class SearchMap(private val max_x: Int, private val max_y: Int, gridMap: Array<Array<Terrain>>){
    private val map: Array<Array<SearchCell>>

    init {
        this.map = (0.rangeTo(max_x).map { y ->
                0.rangeTo(max_y).map { x ->
                    SearchCell(Coordinate(x, y), gridMap[x][y])
                }.toTypedArray()
        }).toTypedArray()
    }

    fun getCell(coordinate: Coordinate): SearchCell {
        if(coordinate.x < 0 || coordinate.x > max_x || coordinate.y < 0 ||coordinate.y > max_y){
            return SearchCell.makeUnmovableCell(coordinate)
        }
        return this.map[coordinate.x][coordinate.y]
    }

    override fun toString(): String {
        return this.map.map { it.map { it.coordinate.x.toString() + ", " + it.coordinate.y + "-" + it.searchPoint } }.toString()
    }
}