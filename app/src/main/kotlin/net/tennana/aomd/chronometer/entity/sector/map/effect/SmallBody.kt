package net.tennana.aomd.chronometer.entity.sector.map.effect

data class SmallBody(val value:Int = 0) {
    fun renewWithAdd(other: SmallBody): SmallBody {
        if(other.value == 0){
            return this;
        }
        return this.copy(value = value + other.value)
    }
}
