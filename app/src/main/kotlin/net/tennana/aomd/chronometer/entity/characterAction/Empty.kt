package net.tennana.aomd.chronometer.entity.characterAction

import net.tennana.aomd.chronometer.entity.characterAction.basic.ActionRequest
import net.tennana.aomd.chronometer.entity.characterAction.basic.CharacterAction
import net.tennana.aomd.chronometer.entity.characterAction.basic.Time
import net.tennana.aomd.chronometer.log.characterAction.CharacterActionResult

class Empty(time: Time) : CharacterAction(time) {
    override fun isSpecified(request: ActionRequest): Boolean {
        // 実行禁止
        return false
    }

    override fun doAction(request: ActionRequest): Result<CharacterActionResult> {
        return Result.failure(Exception("空アクションは実行されてはならない"))
    }
}