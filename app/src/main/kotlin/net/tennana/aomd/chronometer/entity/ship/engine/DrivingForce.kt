package net.tennana.aomd.chronometer.entity.ship.engine

import net.tennana.aomd.chronometer.entity.ship.ComponentDoCommandResult

// 推進力
data class DrivingForce(private val value: Double) : ComponentDoCommandResult, Comparable<Double> {
    fun renewWithAdd(other: DrivingForce): DrivingForce {
        return DrivingForce(value + other.value)
    }

    override fun compareTo(other: Double): Int {
        return this.value.compareTo(other)
    }

    fun decrement(): DrivingForce {
        return this.copy(value = value - 1.0)
    }
}