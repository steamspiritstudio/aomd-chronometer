package net.tennana.aomd.chronometer.entity

import net.tennana.aomd.chronometer.entity.sector.Sector
import net.tennana.aomd.chronometer.entity.sector.Station
import net.tennana.aomd.chronometer.entity.sector.map.Terrain
import net.tennana.aomd.chronometer.entity.ship.ShipPlace

data class Universe(val sector: List<Sector>, val ships: List<Ship>, val stations: List<Station>) {
    fun findSector(sectorId: Long): Sector {
        return sector.find { it.id == sectorId }!!
    }

    fun findStation(stationId: Long): Station {
        return stations.find { it.id == stationId }!!
    }

    fun findShip(shipId: Long): Ship {
        return ships.find { it.id == shipId }!!
    }

    fun findTerrain(place: ShipPlace): Terrain {
        return this.findSector(place.sectorId).map.getTerrain(place.x, place.y)
    }
}