package net.tennana.aomd.chronometer

import com.github.andrewoma.kwery.core.DefaultSession
import com.github.andrewoma.kwery.core.Session
import com.github.andrewoma.kwery.core.dialect.PostgresDialect
import io.github.cdimascio.dotenv.Dotenv
import net.sf.log4jdbc.sql.jdbcapi.DriverSpy
import net.tennana.aomd.chronometer.entity.CurrentTime
import net.tennana.aomd.chronometer.entity.Universe
import net.tennana.aomd.chronometer.log.GameLogger
import net.tennana.aomd.chronometer.log.fightRecorder.FlightRecorderRepository
import net.tennana.aomd.chronometer.logic.CharacterAction
import net.tennana.aomd.chronometer.logic.ShipFlight
import net.tennana.aomd.chronometer.logic.items.ItemIdGenerator
import net.tennana.aomd.chronometer.repository.CharacterRepository
import net.tennana.aomd.chronometer.repository.ShipRepository
import net.tennana.aomd.chronometer.repository.UniverseRepository
import net.tennana.aomd.chronometer.repository.WorldParameters
import net.tennana.aomd.chronometer.repository.WorldParameters.getTurn
import net.tennana.aomd.chronometer.table.ShipActionResultDao
import java.sql.DriverManager
import java.util.*

fun main() {
    // setup
    val dotenv = Dotenv.load();
    TimeZone.setDefault(TimeZone.getTimeZone("Asia/Tokyo"))
    DriverManager.registerDriver(DriverSpy())
    val MAX_AP = dotenv.get("AP_ON_TURN", "12").toInt()
    val connection =
        DriverManager.getConnection(dotenv.get("JDBC_DATABASE_URL"))
    val session = DefaultSession(connection, PostgresDialect())

    ItemIdGenerator.setup(dotenv)

    // start
    session.transaction {
        setupRepositories(session)
        println("現在ターン:" + getTurn() + "フェーズ:" + WorldParameters.getPhaseName())
        val ships = ShipRepository.findAll(session)
        val universe = UniverseRepository.createUniverse(ships)

        if (WorldParameters.isCharacterPhase()) {
            phaseCharacterAction(universe, session, MAX_AP)
        } else {
            phaseShipFlight(universe, session, MAX_AP)
        }
        WorldParameters.next()
        GameLogger.commit(session)

        commitWorldData(universe)
        println("ターン更新完了 / 現在ターン:" + getTurn())
    }
}

private fun phaseCharacterAction(universe: Universe, session: DefaultSession, MAX_AP: Int) {
    val flightRecorders = FlightRecorderRepository(getTurn(), ShipActionResultDao(session)).findAll()
    val logic = CharacterAction(session, getTurn());
    for (rawTime in 0 until MAX_AP) {
        val time = CurrentTime(getTurn(), rawTime)
        logic.doCommand(time, universe, flightRecorders)
    }
    logic.commit()
}

private fun phaseShipFlight(
    universe: Universe,
    session: DefaultSession,
    MAX_AP: Int
) {
    ShipFlight.setup(universe, session)
    for (rawTime in 0 until MAX_AP) {
        val time = CurrentTime(getTurn(), rawTime)
        ShipFlight.doCommand(time, universe)
    }
    ShipFlight.commit()
}

fun setupRepositories(session: Session) {
    WorldParameters.setup(session)
    CharacterRepository.setup(session)
    UniverseRepository.setup(session)
}

fun commitWorldData(universe: Universe) {
    CharacterRepository.commit()
    ShipRepository.commit(universe)
    WorldParameters.commit()
}