package net.tennana.aomd.chronometer.log

import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import net.tennana.aomd.chronometer.log.flight.FlightBasic
import net.tennana.aomd.chronometer.logic.flight.affect.ShipAffect
import java.time.OffsetDateTime

@Serializable
data class ShipActionLog(val id: String = idGenerator(), val flightBasic: FlightBasic, val result: ShipActionResult) {
    fun toRecord(format: Json): ShipActionLogRecord {
        val time = OffsetDateTime.now()
        return ShipActionLogRecord(
            id,
            flightBasic,
            format.encodeToString(result),
            time,
            time
        )
    }

    fun convertAffects(): List<ShipAffect> {
        return result.convertAffects(flightBasic)
    }
}