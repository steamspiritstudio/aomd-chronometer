package net.tennana.aomd.chronometer.log

import net.tennana.aomd.chronometer.log.flight.FlightBasic
import java.time.OffsetDateTime

data class ShipActionLogRecord(
    val id: String,
    val basic: FlightBasic,
    val bodyString: String,
    val createdAt: OffsetDateTime,
    val updatedAt: OffsetDateTime,
) {
}