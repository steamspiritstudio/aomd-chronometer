package net.tennana.aomd.chronometer.entity.ship.engine

import net.tennana.aomd.chronometer.entity.CurrentTime
import net.tennana.aomd.chronometer.entity.ship.basic.ShipComponentImpl
import net.tennana.aomd.chronometer.entity.ship.basic.VoyageComponent
import net.tennana.aomd.chronometer.entity.ship.generator.PowerSupplyCommand

data class Engine(val drivingForce: Int, val maxPower: Int) : ShipComponentImpl(), VoyageComponent {
    public override fun commandPower(): PowerSupplyCommand {
        return PowerSupplyCommand(this, maxPower)
    }

    override fun doCommand(time: CurrentTime): DrivingForce {
        // 獲得電力を推進力へ変換
        val drivingForce = (capacitorPower.toDouble() / maxPower) * drivingForce;
        capacitorPower = 0;
        return DrivingForce(drivingForce)
    }
}