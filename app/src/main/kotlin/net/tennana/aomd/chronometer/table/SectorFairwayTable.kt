package net.tennana.aomd.chronometer.table;

import com.github.andrewoma.kwery.core.Session
import com.github.andrewoma.kwery.mapper.AbstractDao
import com.github.andrewoma.kwery.mapper.Table
import com.github.andrewoma.kwery.mapper.Value
import net.tennana.aomd.chronometer.entity.sector.SectorFairway
import net.tennana.aomd.chronometer.entity.sector.WayPoint

object SectorFairwayTable : Table<SectorFairway, Long>("sector_fairways") {
    val Id by col(SectorFairway::id, id = true)
    val SectorIdAlpha by col(WayPoint::sectorId, SectorFairway::wayPointAlpha)
    val MoveRangeAlpha by col(WayPoint::moveRange, SectorFairway::wayPointAlpha)
    val CentralCoordinateAlphaY by col(WayPoint::y, SectorFairway::wayPointAlpha)
    val CentralCoordinateAlphaX by col(WayPoint::x, SectorFairway::wayPointAlpha)
    val SectorIdBeta by col(WayPoint::sectorId, SectorFairway::wayPointBeta)
    val MoveRangeBeta by col(WayPoint::moveRange, SectorFairway::wayPointBeta)
    val CentralCoordinateBetaY by col(WayPoint::y, SectorFairway::wayPointBeta)
    val CentralCoordinateBetaX by col(WayPoint::x, SectorFairway::wayPointBeta)
    override fun idColumns(id: Long) = setOf(Id of id)
    override fun create(value: Value<SectorFairway>): SectorFairway {
        return SectorFairway(
            value of Id,
            WayPoint(
                value of SectorIdAlpha,
                value of MoveRangeAlpha,
                value of CentralCoordinateAlphaY,
                value of CentralCoordinateAlphaX
            ),
            WayPoint(
                value of SectorIdBeta,
                value of MoveRangeBeta,
                value of CentralCoordinateBetaY,
                value of CentralCoordinateBetaX
            ),
        )
    }
}

class SectorFairwayDao(session: Session) :
    AbstractDao<SectorFairway, Long>(session, SectorFairwayTable, SectorFairway::id)
