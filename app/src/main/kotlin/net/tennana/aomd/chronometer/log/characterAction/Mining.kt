package net.tennana.aomd.chronometer.log.characterAction

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import net.tennana.aomd.chronometer.entity.items.State

@Serializable
@SerialName("Mining")
class Mining(val isSuccess: Boolean, val mItemId: Int, val itemState: State) : CharacterActionResult {
}