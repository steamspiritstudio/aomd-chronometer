package net.tennana.aomd.chronometer.log.characterAction

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("SkillStudy")
class SkillStudy : CharacterActionResult {
}