package net.tennana.aomd.chronometer.logic

import com.github.andrewoma.kwery.core.Session
import net.tennana.aomd.chronometer.entity.CurrentTime
import net.tennana.aomd.chronometer.entity.Universe
import net.tennana.aomd.chronometer.entity.characterAction.Rest
import net.tennana.aomd.chronometer.entity.characterAction.basic.ActionRequest
import net.tennana.aomd.chronometer.entity.characterAction.basic.Time
import net.tennana.aomd.chronometer.log.GameLogger
import net.tennana.aomd.chronometer.log.ShipActionLog
import net.tennana.aomd.chronometer.log.characterAction.CharacterActionBasic
import net.tennana.aomd.chronometer.log.fightRecorder.ShipFlightRecorder
import net.tennana.aomd.chronometer.logic.character.CharacterActionFactory
import net.tennana.aomd.chronometer.logic.character.CharacterActionRepository
import net.tennana.aomd.chronometer.repository.CharacterRepository
import net.tennana.aomd.chronometer.table.PersonalPlanActionDao

class CharacterAction(session: Session, turn: Long) {
    private val repository: CharacterActionRepository

    init {
        this.repository = CharacterActionRepository(PersonalPlanActionDao(session), CharacterActionFactory(), turn)
        this.repository.setup();
    }

    fun doCommand(time: CurrentTime, universe: Universe, flightRecorders: Map<Long, ShipFlightRecorder>) {
        // 船情報はターン開始時のものであるため、1timeごとにログから状態変化を適用する
        flightRecorders.values.forEach {
            val affects = it.find(time).flatMap(ShipActionLog::convertAffects);
            if (affects.isEmpty()) {
                return@forEach
            }
            val ship = universe.findShip(it.shipId);
            ship.affect(affects)
        }

        CharacterRepository.all().forEach { character ->
            val ship = universe.findShip(character.shipId);
            val basic = CharacterActionBasic(character.id, character.shipId, character.placeIdInShip, ship.place, time)
            val actions = repository.findById(character.id)
            val request = ActionRequest(time, character, ship, universe)
            val actionResult = actions.popAction(request).doAction(request).getOrElse {
                // 行動失敗時
                // TODO: ロガーを作ったら置き換える
                println(String.format("Time:%d CharacterId:%d %s", request.time.time, request.character.id, it.message))
                Rest(Time(request.time.turn, 1, request.time.time.toLong())).doAction(request).getOrThrow()
            }
            GameLogger.addLog(basic, actionResult)
        }

    }

    fun commit() {
    }
}