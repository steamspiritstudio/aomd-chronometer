package net.tennana.aomd.chronometer.logic.items

import io.github.cdimascio.dotenv.Dotenv
import net.tennana.aomd.chronometer.entity.CurrentTime
import org.hashids.Hashids


object ItemIdGenerator {
    var count = 0L;
    lateinit var hashids: Hashids;

    fun setup(dotenv: Dotenv) {
        hashids = Hashids(dotenv.get("ITEM_ID_SALT"))
    }

    fun generate(time: CurrentTime): String {
        return hashids.encode(time.turn, time.time.toLong(), count++)
    }
}