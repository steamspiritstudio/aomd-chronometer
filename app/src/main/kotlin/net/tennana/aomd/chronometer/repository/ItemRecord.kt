package net.tennana.aomd.chronometer.repository

import kotlinx.serialization.Serializable

@Serializable
sealed class ItemRecord {
    abstract val id: String;
    abstract val mId: Int;
    abstract val num: Int;
    abstract val quality: Int
}

@Serializable
data class BasicItemRecord(
    override val id: String,
    override val mId: Int,
    override val num: Int,
    override val quality: Int
) : ItemRecord() {

}