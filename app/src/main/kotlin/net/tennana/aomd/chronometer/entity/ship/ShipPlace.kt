package net.tennana.aomd.chronometer.entity.ship

import kotlinx.serialization.Serializable
import net.tennana.aomd.chronometer.entity.Coordinate

@Serializable
data class ShipPlace(val sectorId: Long, val x: Int, val y: Int, val stationId: Long?) {
    fun toCoordinate(): Coordinate {
        return Coordinate(x, y)
    }

    fun stationArrive(stationId: Long): ShipPlace {
        return this.copy(stationId = stationId)
    }

    fun stationLeave() : ShipPlace {
        return this.copy(stationId = null)
    }

    fun inStation() : Boolean {
        return stationId != null && stationId > 0;
    }
}