package net.tennana.aomd.chronometer.repository

import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.modules.polymorphic
import kotlinx.serialization.modules.subclass
import net.tennana.aomd.chronometer.log.ShipActionResult
import net.tennana.aomd.chronometer.log.characterAction.CharacterActionResult
import net.tennana.aomd.chronometer.log.flight.*

val shipActionResultSerializersModule = SerializersModule {
    polymorphic(ShipActionResult::class) {
        subclass(DrivingForceEmpty::class)
        subclass(MovedSector::class)
        subclass(MovedShip::class)
        subclass(NoShipFlightPlan::class)
        subclass(RestShip::class)
        subclass(StationArrive::class)
        subclass(StationLeave::class)
    }
    polymorphic(CharacterActionResult::class) {

    }
}