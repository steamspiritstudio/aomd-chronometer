package net.tennana.aomd.chronometer.entity.characterAction.basic

import net.tennana.aomd.chronometer.entity.CurrentTime
import net.tennana.aomd.chronometer.entity.Ship
import net.tennana.aomd.chronometer.entity.Universe
import net.tennana.aomd.chronometer.entity.character.Character

data class ActionRequest(
    val time: CurrentTime,
    val character: Character,
    val ship: Ship,
    val universe: Universe
) {
}