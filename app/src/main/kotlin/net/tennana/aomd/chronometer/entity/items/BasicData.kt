package net.tennana.aomd.chronometer.entity.items

import kotlinx.serialization.Serializable

@Serializable
data class BasicData(val mId: Int, val name: String, val price: Int, val mass: Int) {
}