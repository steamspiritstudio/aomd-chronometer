package net.tennana.aomd.chronometer.repository

import com.github.andrewoma.kwery.core.Session
import net.tennana.aomd.chronometer.entity.Ship
import net.tennana.aomd.chronometer.entity.Universe
import net.tennana.aomd.chronometer.entity.sector.Sector
import net.tennana.aomd.chronometer.entity.sector.SectorFairways
import net.tennana.aomd.chronometer.entity.sector.Station
import net.tennana.aomd.chronometer.entity.sector.map.GridMap
import net.tennana.aomd.chronometer.table.*

const val MAX_MAP_SIZE = 25;

object UniverseRepository {
    private lateinit var allSector: List<Sector>
    private lateinit var allStation: List<Station>

    fun setup(session: Session) {
        val sectorDao = SectorOfSpaceDao(session)
        val fairwayDao = SectorFairwayDao(session)
        val sectorObjectDao = SectorObjectDao(session)

        this.allStation = StationDao(session).findAll()
        val sectorRecords = sectorDao.findAll()
        val fairwayRecords = fairwayDao.findAll()
        val objectRecords = sectorObjectDao.findAll()

        this.allSector = sectorRecords.map { sectorRecord ->
            Sector(
                sectorRecord.id,
                sectorRecord.name,
                GridMap(MAX_MAP_SIZE, MAX_MAP_SIZE, emptyList()),
                SectorFairways(
                    sectorRecord.id,
                    fairwayRecords.filter { sectorFairway -> sectorFairway.wayPointAlpha.sectorId == sectorRecord.id || sectorFairway.wayPointBeta.sectorId == sectorRecord.id }
                )
            )
        }
    }

    fun createUniverse(ships: List<Ship>): Universe {
        return Universe(this.allSector, ships, this.allStation)
    }
}