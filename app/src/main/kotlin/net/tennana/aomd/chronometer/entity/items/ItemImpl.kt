package net.tennana.aomd.chronometer.entity.items

import net.tennana.aomd.chronometer.repository.BasicItemRecord
import net.tennana.aomd.chronometer.repository.ItemRecord

abstract class ItemImpl(val id: String, val basic: BasicData, var state: State) {
    public fun toItemRecord(): ItemRecord {
        return BasicItemRecord(id, basic.mId, state.hasNum, state.quality);
    }

    fun isSame(other: ItemImpl): Boolean {
        // 同じ所持者、同じ品質なら同一アイテムと見なす(品質が異なるなら別アイテム)
        return basic.mId == other.basic.mId && state.owner == other.state.owner && state.quality == other.state.quality
    }

    fun ordinate(other: ItemImpl): Result<ItemImpl> {
        if (!isSame(other)) {
            return Result.failure(Exception("まとめられないアイテム"));
        }
        this.state = this.state.copy(hasNum = this.state.hasNum + other.state.hasNum)
        return Result.success(this);
    }
}