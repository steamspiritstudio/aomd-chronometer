package net.tennana.aomd.chronometer.entity.sector

import net.tennana.aomd.chronometer.entity.sector.map.GridMap

data class Sector(val id: Long, val name: String, val map: GridMap, val fairways: SectorFairways)
