package net.tennana.aomd.chronometer.entity.ship.generator

import net.tennana.aomd.chronometer.entity.CurrentTime
import net.tennana.aomd.chronometer.entity.ship.ComponentDoCommandResult
import net.tennana.aomd.chronometer.entity.ship.VoidResult
import net.tennana.aomd.chronometer.entity.ship.basic.VoyageComponent

data class Generators(private val generators: List<Generator>, private var storagePower: Int = 0): VoyageComponent {
    private var supplyResult = 0;

    fun doCommand(time: CurrentTime): ComponentDoCommandResult {
        // v0.1では燃料は考慮しないため、そのまま電力が得られる
        supplyResult = generators.fold(0) { sum, generator -> sum + generator.supply }
        storagePower += supplyResult
        return VoidResult()
    }

    fun request(command: PowerSupplyCommand): PowerSupplyCommand {
        val supply = command.required.coerceAtMost(storagePower)
        command.supply(supply)
        storagePower -= supply
        return command
    }

    fun getMass(): Int {
        return this.generators.fold(0) { sum, generator -> sum + generator.getMass() }
    }

    fun getSurplusPower(): Int {
        return storagePower;
    }

    fun getSupplyPower(): Int {
        return supplyResult;
    }
}