package net.tennana.aomd.chronometer.log.flight

import kotlinx.serialization.Serializable
import net.tennana.aomd.chronometer.entity.CurrentTime
import net.tennana.aomd.chronometer.entity.Ship

/**
 * 宇宙船行動ログ基本情報
 */
@Serializable
data class FlightBasic(
    val turn: Long,
    val time: Long,
    val shipId: Long,
    val shipName: String,
    val supplyPower: Int,
    val surplusPower: Int,
    // 移動前の位置情報
    val sectorId: Long,
    val x: Int,
    val y: Int,
    val stationId: Long?
) {
    constructor(
        time: CurrentTime,
        ship: Ship
    ) : this(
        time.turn,
        time.time.toLong(),
        ship.id,
        ship.visual.name,
        ship.components.generators.getSupplyPower(),
        ship.components.generators.getSurplusPower(),
        ship.place.sectorId,
        ship.place.x,
        ship.place.y,
        ship.place.stationId
    )
}