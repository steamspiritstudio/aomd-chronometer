package net.tennana.aomd.chronometer.entity.sector.map

import net.tennana.aomd.chronometer.entity.sector.SectorFairway

data class TerrainFlags(val flags: List<TerrainFlag> = arrayListOf()) : Iterable<TerrainFlag> {
    override fun iterator(): Iterator<TerrainFlag> {
        return flags.iterator()
    }
}

interface TerrainFlag {

}

data class Fairway(val sectorId: Long, private val fairway: SectorFairway) : TerrainFlag {
}