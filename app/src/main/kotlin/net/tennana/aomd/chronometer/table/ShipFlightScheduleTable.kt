package net.tennana.aomd.chronometer.table;

import com.github.andrewoma.kwery.core.Session
import com.github.andrewoma.kwery.mapper.AbstractDao
import com.github.andrewoma.kwery.mapper.Table
import com.github.andrewoma.kwery.mapper.Value
import com.github.andrewoma.kwery.mapper.VersionedWithTimestamp
import net.tennana.aomd.chronometer.entity.Coordinate
import net.tennana.aomd.chronometer.entity.ship.MoveSector
import net.tennana.aomd.chronometer.entity.ship.ShipFlightScheduleRecord
import net.tennana.aomd.chronometer.entity.ship.StationApproach

object ShipFlightScheduleTable : Table<ShipFlightScheduleRecord, Long>("ship_flight_schedules"),
    VersionedWithTimestamp {
    val Id by col(ShipFlightScheduleRecord::id, id = true)
    val ShipId by col(ShipFlightScheduleRecord::shipId)
    val ToSectorId by col(ShipFlightScheduleRecord::toSectorId)
    val ToStationId by col(ShipFlightScheduleRecord::toStationId)
    val CoordinateX by col(ShipFlightScheduleRecord::coordinateX, default = 0)
    val CoordinateY by col(ShipFlightScheduleRecord::coordinateY, default = 0)
    val WaitAp by col(ShipFlightScheduleRecord::waitAp)
    val RegisterAccountId by col(ShipFlightScheduleRecord::registerAccountId)
    val RegisterCharacterId by col(ShipFlightScheduleRecord::registerCharacterId)
    val CreatedAt by col(ShipFlightScheduleRecord::createdAt)
    val UpdatedAt by col(ShipFlightScheduleRecord::updatedAt, version = true)
    override fun idColumns(id: Long) = setOf(Id of id)
    override fun create(value: Value<ShipFlightScheduleRecord>): ShipFlightScheduleRecord {
        if (((value of ToStationId) ?: 0) > 0) {
            return StationApproach(
                value of Id,
                value of ShipId,
                value of WaitAp,
                (value of ToStationId)!!,
                value of RegisterAccountId,
                value of RegisterCharacterId,
                value of CreatedAt,
                value of UpdatedAt,
            )
        }
        val coordinate =  Coordinate((value of CoordinateX)?.toInt() ?: 0, (value of CoordinateY)?.toInt() ?: 0);
        return MoveSector(
            value of Id,
            value of ShipId,
            value of WaitAp,
            (value of ToSectorId) ?: 0,
            coordinate,
            value of RegisterAccountId,
            value of RegisterCharacterId,
            value of CreatedAt,
            value of UpdatedAt,
        )
    }
}

class ShipFlightScheduleDao(session: Session) :
    AbstractDao<ShipFlightScheduleRecord, Long>(session, ShipFlightScheduleTable, ShipFlightScheduleRecord::id)
