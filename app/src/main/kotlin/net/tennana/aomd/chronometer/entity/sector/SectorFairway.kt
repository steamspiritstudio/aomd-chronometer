package net.tennana.aomd.chronometer.entity.sector

import net.tennana.aomd.chronometer.entity.Coordinate
import net.tennana.aomd.chronometer.entity.ship.ShipPlace
import kotlin.math.absoluteValue

data class WayPoint(val sectorId: Long, val moveRange: Long, val x: Long, val y: Long) {
    fun toCoordinate(): Coordinate {
        return Coordinate(x.toInt(), y.toInt())
    }

    fun inRange(place: ShipPlace): Boolean {
        // (coordinate.x - @x).abs <= range and (coordinate.y - @y).abs <= range
        return (place.x - x).absoluteValue <= moveRange && (place.y - y).absoluteValue <= moveRange;
    }
}

data class SectorFairway(
    val id: Long,
    val wayPointAlpha: WayPoint,
    val wayPointBeta: WayPoint,
) {
    fun getWayPointTo(ToSectorId: Long): WayPoint {
        if (wayPointAlpha.sectorId == ToSectorId) {
            return wayPointAlpha
        }
        return wayPointBeta
    }

    fun getWayPointFrom(currentSectorId: Long): WayPoint {
        if (wayPointAlpha.sectorId == currentSectorId) {
            return wayPointAlpha
        }
        return wayPointBeta
    }
}