package net.tennana.aomd.chronometer.logic

import com.github.andrewoma.kwery.core.Session
import net.tennana.aomd.chronometer.entity.Coordinate
import net.tennana.aomd.chronometer.entity.CurrentTime
import net.tennana.aomd.chronometer.entity.Ship
import net.tennana.aomd.chronometer.entity.Universe
import net.tennana.aomd.chronometer.entity.sector.map.SearchCell
import net.tennana.aomd.chronometer.entity.ship.ShipFlightScheduleRecord
import net.tennana.aomd.chronometer.entity.ship.ShipPlace
import net.tennana.aomd.chronometer.entity.ship.engine.DrivingForce
import net.tennana.aomd.chronometer.log.GameLogger
import net.tennana.aomd.chronometer.log.flight.*
import net.tennana.aomd.chronometer.logic.flight.ShipFlightScheduleRepository
import net.tennana.aomd.chronometer.logic.flight.affect.ShipAffect
import net.tennana.aomd.chronometer.logic.flight.makeNormalRoute

object ShipFlight {
    private const val MOVE_ONE_CELL = 1.0;

    private lateinit var scheduleRepository: ShipFlightScheduleRepository
    private val commandedId = hashSetOf<Long>()
    private val updatedRecord = hashMapOf<Long, ShipFlightScheduleRecord>()

    fun setup(universe: Universe, session: Session) {
     this.scheduleRepository = ShipFlightScheduleRepository.setup(session)
    }

    fun doCommand(time: CurrentTime, universe: Universe) {
        universe.ships.forEach {
            // 発電機動作(行動計画の有無に関わらず発電は行われる)
            it.components.generators.doCommand(time)
            val flightBasic = FlightBasic(time, it)
            if (scheduleRepository.isEmpty(it.id)) {
                // 最小消費で電力要求
                it.components.powerSupply(time)
                // 航行計画が空
                GameLogger.addLog(flightBasic, NoShipFlightPlan())
                return@forEach
            }
            val plan = scheduleRepository.nextPlan(it.id)
            val to = plan.toCoordinate(universe, it.place.sectorId)
            val currentSector = universe.findSector(it.place.sectorId)
            val route = makeNormalRoute(currentSector.map.convertToSearchMap(), it.place.toCoordinate(), to)
            if (route.isEmpty() || it.place.toCoordinate() == to) {
                // 移動しない
                // 最小消費で電力要求
                it.components.powerSupply(time)
                GameLogger.addLog(flightBasic, RestShip())
                scheduleRepository.decrementWaitAp(plan)
                return@forEach
            }
            // 電力要求してから推進変換
            it.components.powerSupply(time)
            val drivingForce = chargeDrivingForce(time, it)
            if (drivingForce < MOVE_ONE_CELL) {
                // 移動に必要な推進力が出ない
                GameLogger.addLog(flightBasic, DrivingForceEmpty())
                return@forEach
            }
            doFlightPlan(it, time, universe, drivingForce, route, plan)
        }
    }

    private fun doFlightPlan(
        ship: Ship,
        time: CurrentTime,
        universe: Universe,
        initDrivingForce: DrivingForce,
        initRoute: java.util.ArrayList<SearchCell>,
        initPlan: ShipFlightScheduleRecord
    ) {
        var drivingForce = initDrivingForce
        var route = initRoute
        var plan = initPlan
        if (ship.place.inStation()) {
            // ステーション離陸処理
            GameLogger.addLog(FlightBasic(time, ship), StationLeave())
            ship.place = ship.place.stationLeave()
        }
        do {
            // MEMO: 追加で電力を消費する行動を実装した場合、残電力を更新する必要がある
            val flightBasic = FlightBasic(time, ship)
            val flightResult = doFlight(time, ship, drivingForce, route);
            ship.affect(flightResult);
            drivingForce = flightResult.drivingForce
            // 計画の目的を達したか判定・処理
            val completeResult = plan.doArrive(universe, time, ship)
            // 結果ログ追加
            GameLogger.addLog(flightBasic, completeResult.getOrDefault(MovedShip(ship.place)))
            if (completeResult.isFailure && !flightResult.emptyRoute) {
                // 旅程が残っている
                break
            }
            if (
                // 待機時間が残っているか
                !scheduleRepository.decrementWaitAp(plan) ||
                // 推進力が残っていないか
                flightResult.drivingForce < MOVE_ONE_CELL ||
                // 次の旅程がない
                scheduleRepository.isEmpty(ship.id)
            ) {
                break;
            }
            // 次の航行計画を初期化してループ
            plan = scheduleRepository.nextPlan(ship.id);
            val nextTo = plan.toCoordinate(universe, ship.place.sectorId)
            val currentSector = universe.findSector(ship.place.sectorId)
            route = makeNormalRoute(currentSector.map.convertToSearchMap(), ship.place.toCoordinate(), nextTo)
        } while (drivingForce >= MOVE_ONE_CELL)
    }

    private fun chargeDrivingForce(currentTime: CurrentTime, ship: Ship): DrivingForce {
        var drivingForce = ship.components.engines.doCommand(currentTime);
        val mass = ship.components.getMass();
        // TODO: 操縦スキルを反映する
        // 質量を推進力から減算
        drivingForce = drivingForce.renewWithAdd(DrivingForce((-mass).toDouble()))
        // TODO: 引力を反映する
        return drivingForce
    }

    private fun doFlight(
        currentTime: CurrentTime,
        ship: Ship,
        _drivingForce: DrivingForce,
        _route: List<SearchCell>
    ): FlightResult {
        var drivingForce = _drivingForce
        var route = ArrayList(_route)
        var lastCoordinate = ship.place.toCoordinate()
        // 推進力1で1マス
        while (drivingForce >= MOVE_ONE_CELL) {
            val cell = route.removeFirstOrNull();
            if (cell == null) {
                break
            }
            // MEMO: 地形進入時に処理を行うならここで行う
            lastCoordinate = cell.coordinate
            drivingForce = drivingForce.decrement();
        }
        return FlightResult(drivingForce, lastCoordinate, route.isEmpty())
    }

    fun commit() {
        scheduleRepository.commit()
    }
}

data class FlightResult(val drivingForce: DrivingForce, val lastCoordinate: Coordinate, val emptyRoute: Boolean) :
    ShipAffect.PlaceAffect {
    override fun doCommand(place: ShipPlace): ShipPlace {
        return place.copy(x = lastCoordinate.x, y = lastCoordinate.y)
    }
}