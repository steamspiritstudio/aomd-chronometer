package net.tennana.aomd.chronometer.log.fightRecorder

import net.tennana.aomd.chronometer.entity.CurrentTime
import net.tennana.aomd.chronometer.log.ShipActionLog

data class ShipFlightRecorder(val shipId: Long, private val shipActionLogList: Map<Long, List<ShipActionLog>>) {
    fun find(currentTime: CurrentTime): List<ShipActionLog> {
        val time = currentTime.time.toLong()
        return shipActionLogList.getOrDefault(time, listOf())
    }
}