package net.tennana.aomd.chronometer.entity.items

import net.tennana.aomd.chronometer.repository.ItemRecord

class Items(private var items: List<ItemImpl>) {
    fun toDbMap(): List<ItemRecord> {
        return items.map { it.toItemRecord() }
    }

    fun findOne(mId: Int): ItemImpl? {
        return items.find { it.basic.mId == mId }
    }

    fun addItem(newItem: ItemImpl) {
        items += newItem
    }
}
